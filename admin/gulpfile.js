var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	templateCache = require('gulp-angular-templatecache'),
	ngAnnotate = require('gulp-ng-annotate'),
	gls = require('gulp-live-server'),

	buildIndex = require('./build/build-index');

// Scripts \\
gulp.task('script', function() {
	return gulp.src(['app/l2-module.js', 'app/**/*.js'])
		.pipe(gulp.dest('dist/app'));
});
gulp.task('script:minify', function() {
	return gulp.src(['app/l2-module.js', 'app/**/*.js'])
		.pipe(ngAnnotate())
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist'));
});

// Static Assets \\
gulp.task('assets', function() {
	return gulp.src('assets/**/*.*')
		.pipe(gulp.dest('dist/assets'));
});

// Index.html \\
gulp.task('index', function() {
	return gulp.src(['app/index.html', 'app/**/*.js'])
		.pipe(buildIndex())
		.pipe(gulp.dest('dist/index.html'));
});
gulp.task('index:minify', function() {
	return gulp.src(['app/index.html', 'app/**/*.js'])
		.pipe(buildIndex({minified: './app.js'}))
		.pipe(gulp.dest('dist'));
});

// Templates \\
gulp.task('templates', function() {
	return gulp.src('app/*/**/*.html')
		.pipe(templateCache({module: 'l2', root: 'app/'}))
		.pipe(gulp.dest('dist'));
});

// Vendor \\
gulp.task('vendor:js', function() {
	return gulp.src([
			'bower_components/jquery/dist/jquery.min.js',
			'bower_components/cookies-js/dist/cookies.min.js',
			'bower_components/angular/angular.min.js',
			'bower_components/angular-route/angular-route.min.js',
			'bower_components/angular-messages/angular-messages.min.js'
		])
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest('dist/vendor'));
});
gulp.task('vendor:style', function() {
	return gulp.src([
			'bower_components/Ink/dist/css/ink.min.css',
			'bower_components/font-awesome/css/font-awesome.min.css'
		])
		.pipe(concat('vendor.css'))
		.pipe(gulp.dest('dist/vendor'));
});
gulp.task('vendor:assets', function() {
	return gulp.src([
			'bower_components/font-awesome/fonts/*.*',
			'bower_components/Ink/dist/fonts/**/*.*'
		])
		.pipe(gulp.dest('dist/fonts'));
});

// Groups \\
gulp.task('app', [
	'script',
	'index',
	'templates',
	'assets',
	'vendor']);
gulp.task('app:minify', [
	'script:minify',
	'index:minify',
	'templates',
	'assets',
	'vendor']);
gulp.task('vendor', [
	'vendor:js',
	'vendor:style',
	'vendor:assets']);


// Watch \\
gulp.task('watch', function() {
	var server = gls.new('build/api-runner.js');

	// Start watchers
	gulp.watch('app/**/*.js', ['script']);
	gulp.watch(['app/index.html', 'app/**/*.js'], ['index']);
	gulp.watch('app/*/**/*.html', ['templates']);
	gulp.watch('assets/**/*.*', ['assets']);
	
	gulp.watch('../src/**/*.js', function() {
		return server.stop().then(function() {
			return server.start();
		});
	});

	// Initial build and start
	gulp.start('app');
	server.start();
});
gulp.task('watch:minify', function() {
	var server = gls.new('build/api-runner.js');

	// Start watchers
	gulp.watch('app/**/*.js', ['script:minify']);
	gulp.watch(['app/index.html', 'app/**/*.js'], ['index:minify']);
	gulp.watch('app/*/**/*.html', ['templates']);
	gulp.watch('assets/**/*.*', ['assets']);
	
	gulp.watch('../src/**/*.js', function() {
		return server.stop().then(function() {
			return server.start();
		});
	});

	// Initial build and start
	gulp.start('app:minify');
	server.start();
});
