/* API Runner
 *    started by the start-api.js script to run an API
 */
var l2 = require('../../src'),

	port = 4200,
	api = new l2.API();

// Start the server
api.listen(port).then(function() {
	// Notify the start
	console.log('Started level2-remix on port ' + port);

}).done();
