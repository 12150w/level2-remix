/* Gulp Index Merger
 *    generates index.html with all the required
 *    includes
 */
var Transform = require('stream').Transform,
	util = require('util');

util.inherits(IndexBuilder, Transform);
function IndexBuilder(options) {
	if(this instanceof IndexBuilder !== true) return new IndexBuilder(options);

	options = options || {};
	options.objectMode = true;
	Transform.call(this, options);

	options.spacer = "\t\t";

	this._options = options;
	this._includeSrc = '';
	this._template = null;
	this._templateEncoding = null;
};

IndexBuilder.prototype._transform = function(file, encoding, cb) {

	// Check if this is a JS file
	var isJS = /\.js$/.exec(file.relative) !== null;
	if(isJS === true && !this._options.minified) {
		this._includeSrc = '<script src="app/' + file.relative + '"></script>\n' + this._options.spacer + this._includeSrc;
	}

	// Check for index.html
	if(file.relative === 'index.html') {
		this._template = file;
		this._templateEncoding = encoding;
	}

	cb();
};

IndexBuilder.prototype._flush = function(cb) {
	if(this._template === null) return cb(new Error('No index.html found from input'));

	// Add livereload
	if(!this._options.minified) {
		this._includeSrc += '<script src="http://127.0.0.1:35729/livereload.js"></script>\n' + this._options.spacer;
	}

	// Add minified
	if(!!this._options.minified) {
		this._includeSrc += '<script src="' + this._options.minified + '"></script>\n' + this._options.spacer;
	}
	this._includeSrc += '<script src="./templates.js"></script>\n' + this._options.spacer;

	// Merge the output
	var source = this._template.contents.toString(this._templateEncoding);
	source = source.replace('{{BUILDER_APP_INCLUDE}}', this._includeSrc);
	this._template.contents = new Buffer(source, this._templateEncoding);

	// Move the index.html file
	if(!this._options.minified) {
		this._template.path = this._template.path.replace(/[^\/]+\/index\.html$/, '/index.html');
	}
	this.push(this._template);

	cb();
};

module.exports = IndexBuilder;
