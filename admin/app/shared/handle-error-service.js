angular.module('l2')
.factory('$exceptionHandler', function($injector) {
	var $rootScope, $location, api;
	
	return function(err) {
		$rootScope = $rootScope || $injector.get('$rootScope');
		$location = $location || $injector.get('$location');
		api = api || $injector.get('api');
		
		// Set up the scope error
		$rootScope.userError = err;
		
		// Check if this error should cause the redirect
		var doRedirect = true;
		if(err instanceof api.APIError && $rootScope.formProcess === true) {
			doRedirect = false;
			$rootScope.formProcess = false;
		}
		
		if(doRedirect === true) {
			$location.url('/error');
		}
	};
});