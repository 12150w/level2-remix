angular.module('l2')
.directive('l2Checkbox', function() {
	var link = function(scope, element, attrs) {
		scope.toggle = function() {
			scope.checked = !scope.checked
		};
	};
	
	return {
		link: link,
		scope: {
			checked: '=checked'
		},
		templateUrl: 'app/shared/inputs/checkbox.html'
	};
});
