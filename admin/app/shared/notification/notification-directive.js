angular.module('l2').directive('l2Notifications', function(notification) {
	
	var linker = function(scope, element, attrs, controller, transclude) {
		
		// Empty the notifications
		scope.alerts = [];
		
		// Add new notification listener
		notification.onAlert(function(alert) {
			scope.alerts.push(alert);
		});
		
		// Remove listener
		scope.removeAlert = function(removeIdx) {
				if(typeof(removeIdx) === 'number') scope.alerts.splice(removeIdx, 1);
			};
		
		transclude(scope, function(clone) {
			element.append(clone);
		});
	};
	
	return {
		templateUrl: 'app/shared/notification/notification-template.html',
		transclude: true,
		link: linker,
		scope: {
		}
	}
});