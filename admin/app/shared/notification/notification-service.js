angular.module('l2').service('notification', function() {
	var notification = {};
	
	/* Add Message
	 *    Underlying add notification function
	 * 
	 * data:
	 *    message string: notification text
	 *    [prefix] string: bold text
	 *    severity string: class to change the color
	 */
	var addMessage = function(data) {
		if(typeof(onAlertHandler) !== 'function') return;
		onAlertHandler(data);
	};
	
	/* Success
	 *    Adds a success notification
	 */
	notification.success = function(message, prefix) {
		addMessage({
			message: message,
			prefix: prefix,
			severity: 'success'
		});
	};
	
	/* On Alert
	 *    Sets the alert notification function
	 */
	var onAlertHandler;
	notification.onAlert = function(handler) {
		onAlertHandler = handler;
	};
	
	return notification;
});