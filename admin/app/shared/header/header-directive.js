angular.module('l2')
.directive('l2Header', function($rootScope, $location) {

	// Add the routeTo function
	$rootScope.routeTo = function(url) {
		$location.url(url);
	};

	return {
		templateUrl: 'app/shared/header/header-template.html'
	};
});
