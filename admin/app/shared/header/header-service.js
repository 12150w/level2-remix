angular.module('l2')
.service('header', function($rootScope) {
	var header = {};
	
	/* Set Page Title
	 *    Sets the page title
	 */
	header.setPageTitle = function(title) {
		$rootScope.pageTitle = title;
	};
	
	return header;
});