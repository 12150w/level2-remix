angular.module('l2')
.directive('l2Matches', function() {
	return {
		scope: {
			otherVal: '=l2Matches'
		},
		require: 'ngModel',
		link: function(scope, element, attributes, controller) {
			controller.$validators.matches = function(thisVal) {
				return thisVal == scope.otherVal;
			};
			
			scope.$watch('otherVal', function() {
				controller.$validate();
			});
		}
	}
});