angular.module('l2')
.service('auth', function() {
	var auth = {},
		currentSID = null,
		sidKey = 'l2-admin-session';
	
	/* Set Session
	 *    Sets the session SID
	 */
	auth.setSession = function(sid) {
		currentSID = sid;
		Cookies.set(sidKey, sid);
	};
	
	/* Get Session
	 *    Returns the cached SID
	 */
	auth.getSession = function() {
		return currentSID;
	};
	
	/* Reload Session
	 *    Reads the session SID from the Cookies
	 */
	auth.reloadSession = function() {
		var readSID = Cookies.get(sidKey);
		if(!readSID) {
			currentSID = null;
		} else {
			currentSID = readSID;
		}
	};
	auth.reloadSession();
	
	/* Clear Session
	 *    Unsets the stored SID
	 */
	auth.clearSession = function() {
		Cookies.expire(sidKey);
		currentSID = null;
	}
	
	return auth;
});