angular.module('l2')
.filter('tostring', function() {
	return function(input) {
		return String(input);
	};
});