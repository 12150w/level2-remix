angular.module('l2')
.directive('l2Icon', function() {
	var link = function(scope, element, attrs) {
		var iconName = attrs.l2Icon;
		
		if(!element.hasClass('fa')) {
			element.addClass('fa');
		}
		element.addClass(iconName);
	};
	
	return {
		link: link
	};
});