angular.module('l2')
.directive('l2Spin', function() {
	var link = function(scope, element, attrs) {
		if(!element.hasClass('fa')) {
			element.addClass('fa');
		}
		element.addClass('fa-spin');
	};
	
	return {
		link: link
	};
});