angular.module('l2')
.directive('l2Form', function(api, $q, $rootScope) {
	var linker = function(scope, element, attrs, controller, transclude) {
		scope.data = scope.data || {};
		scope.error = {
			message: null,
			details: null
		};
		scope.loading = false;

		// Form submission
		scope.submitForm = function() {

			// Check for client validation error
			if(scope.form.$invalid) {
				return;
			}

			scope.error.message = null;
			scope.error.details = null;
			scope.loading = true;

			// Check for pre submit
			var preparePromise;
			if(typeof(scope.preSubmit) === 'function') {
				var prepareData = scope.preSubmit(scope.data) || {};
				preparePromise = prepareData;

				if(typeof(preparePromise.then) !== 'function') {
					preparePromise = $q(function(resolve, reject) {
						resolve(prepareData);
					});
				}
			} else {
				preparePromise = $q(function(resolve, reject) {
					resolve(scope.data);
				});
			}

			// POST
			var start = (new Date()).getTime(),
				postData;
			$rootScope.formProcess = true;
			preparePromise.then(function(transformedData) {
				// POST the data
				postData = transformedData;
				return api.post(scope.url, postData);

			}).finally(function(res) {
				// Check for UI timeout
				var diff = (new Date()).getTime() - start;
				if(diff < 400) {
					var timeoutDefer = $q.defer();
					setTimeout(function() {
						timeoutDefer.resolve(res);
					}, 400 - diff);

					return timeoutDefer.promise;
				} else {
					return res;
				}

			}).then(function(res) {
				// Run the success handler if needed
				if(typeof(scope.successHandler) === 'function') {
					scope.successHandler(res);
				}

			}, function(err) {
				// Display an error message
				scope.error.message = err.data.msg;

			}).finally(function() {
				// Hide the loading message
				scope.loading = false;
				$rootScope.formProcess = false;

			});
		};

		// Transclude with the directive scope
		transclude(scope, function(clone) {
			element.find('input[type="submit"]').before(clone);
		});
	};

	return {
		scope: {
			url: '@url',
			submitText: '@submitText',
			name: '@name',
			successHandler: '=?onSuccess',
			preSubmit: '=?preSubmit',
			data: '=?data'
		},
		link: linker,
		templateUrl: 'app/shared/api/form-template.html',
		transclude: true
	};
});
