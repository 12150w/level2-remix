angular.module('l2')
.service('api', function($http, $location, auth) {
	var api = {};
	
	/* Request
	 *    Sends an HTTP request to the server
	 * 
	 * url is relative to the API root but must start with /
	 * options contains information about the request including
	 *    options.params: url parameters
	 *    options.body: request body
	 *    options.headers: additional request headers
	 */
	api.request = function(method, url, options) {
		var cfg = {
			method: method,
			url: '..' + url
		};
		options = options || {};
		
		// Add the body if available
		if(!!options.body) {
			if(typeof(options.body) === 'string') cfg.data = options.body;
			else cfg.data = JSON.stringify(options.body);
		}
		
		// Add in any URL parameters
		if(!!options.params) {
			cfg.params = options.params;
		}
		
		// Set up the headers
		cfg.headers = {};
		if(!!options.headers) {
			for(var headerName in options.headers) {
				cfg.headers[headerName] = options.headers[headerName];
			}
		}
		
		// Set Authorization header
		var authSession = auth.getSession();
		if(authSession !== null) {
			cfg.headers['Authorization'] = authSession;
		}
		
		return $http(cfg).error(function(body, httpCode) {
			
			// Check for the login required code
			if(body.code === 'NO_LOGIN') {
				auth.clearSession();
				$location.url('/');
				return;
			}
			
			throw new api.APIError(body.msg, body.code, body.stack);
		});
	};
	
	/* Get
	 *    Sends a HTTP GET request to the api
	 */
	api.get = function(url) {
		return api.request('GET', url);
	};
	
	/* Post
	 *    Sends a HTTP POST request to the api
	 */
	api.post = function(url, body) {
		return api.request('POST', url, {
			body: body
		});
	};
	
	/* APIError
	 * 
	 */
	api.APIError = function(msg, code, stack) {
		this.msg = msg;
		this.message = msg;
		
		this.code = code;
		this.apiStack = stack;
	};
	api.APIError.prototype = new Error();
	
	return api;
});