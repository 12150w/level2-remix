angular.module('l2').config(function($routeProvider) {

    $routeProvider.when('/account/request-reset', {
        templateUrl: 'app/components/account/request-reset-template.html',
        controller: 'RequestResetController'
    });
	
	$routeProvider.when('/reset-password/:prrCode', {
		templateUrl: 'app/components/account/reset-password-template.html',
        controller: 'ResetPasswordController'
	});

});
