angular.module('l2')
.controller('ResetPasswordController', function($scope, $routeParams) {
	$scope.showForm = true;
	$scope.resetData = {
		code: $routeParams.prrCode
	};
	
	$scope.onReset = function() {
		$scope.showForm = false;
	};
});