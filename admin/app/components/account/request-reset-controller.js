angular.module('l2')
.controller('RequestResetController', function($scope) {
	$scope.showForm = true;
	
	$scope.showEmailSent = function() {
		$scope.showForm = false;
	};
});