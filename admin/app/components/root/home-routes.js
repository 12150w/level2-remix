angular.module('l2').config(function($routeProvider) {
	// Root
	$routeProvider.when('/', {
		templateUrl: 'app/components/root/home.html',
		controller: 'HomeController',
		resolve: {
			setupStatus: function(setup) { return setup.getStatus(); }
		}
	});
});