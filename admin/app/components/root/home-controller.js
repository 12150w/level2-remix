angular.module('l2')
.controller('HomeController', function(setupStatus, auth, $location, $scope) {
	
	// Check for setup step
	if(setupStatus === null) {
		$location.url('/setup/db');
	} else if(setupStatus === 'db') {
		$location.url('/setup/admin');
	}
	
	// Check for already logged in
	if(auth.getSession() !== null) {
		$location.url('/panel');
	}
	
	$scope.startSession = function(res) {
		auth.setSession(res.data.sid);
		$location.url('/panel');
	};
});