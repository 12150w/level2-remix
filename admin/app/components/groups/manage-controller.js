angular.module('l2')
.controller('GroupManageController', function(allGroups, $scope, api) {
	$scope.groups = allGroups.data.groups;
	$scope.newData = {};
	
	$scope.injectGroup = function() {
		var newKey = $scope.newData.key;
		$scope.groups.push({key: newKey});
		
		$scope.newData.key = null;
	};
	
	$scope.deleteGroup = function(key, index) {
		api.post('/users/delete-group', {
			key: key
		}).then(function() {
			window.location.reload();
		});
	};
});
