angular.module('l2').config(function($routeProvider) {
	
	// Panel Home
	$routeProvider.when('/groups/manage', {
		templateUrl: 'app/components/groups/manage.html',
		controller: 'GroupManageController',
		resolve: {
			allGroups: function(api) {
				return api.get('/users/all-groups');
			}
		}
	});
	
});
