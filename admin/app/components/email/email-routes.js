angular.module('l2').config(function($routeProvider) {

    $routeProvider.when('/email/settings', {
        templateUrl: 'app/components/email/email-settings-template.html',
        controller: 'EmailSettingsController',
        resolve: {
            emailSettings: function(api) {
                return api.get('/email/settings').then(function(res) {
                    var model = {
                        transport: res.data.transport,
                    };

                    res.data.options = res.data.options || {};
                    for(var key in res.data.options) {
                        if(key === 'transport') continue;
                        model[key] = res.data.options[key];
                    }

					model.defaultFrom = res.data.defaultFrom;
                    model.externalUrl = res.data.externalUrl;

                    return model;
                });
            }
        }
    });

});
