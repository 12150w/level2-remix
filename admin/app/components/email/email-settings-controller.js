angular.module('l2')
.controller('EmailSettingsController', function($scope, emailSettings) {
    $scope.emailSettings = emailSettings;

    $scope.setupData = function(data) {
        data = JSON.parse(JSON.stringify(data));
		var postData = {};

		// Set transport options
        switch(data.transport) {

            case 'smtp':
                postData.transport = 'smtp';

                postData.options = JSON.parse(JSON.stringify(data));

				delete postData.options.transport;
				delete postData.options.options;
				delete postData.options.defaultFrom;

                if(!!postData.options.port) {
                    postData.options.port = parseInt(postData.options.port, 10);
                    if(isNaN(postData.options.port)) delete postData.options.port;
                }

                break;

            case 'debug':
                postData.transport = 'debug';
				postData.options = null;
				break;

            case 'direct':
                postData.transport = 'direct';
				postData.options = null;
				break;

            default:
                postData = {};
				break;

        }

		if(!!data.defaultFrom) {
			postData.defaultFrom = data.defaultFrom;
		} else {
			delete postData.defaultFrom;
		}
        
        if(!!data.externalUrl) {
            postData.externalUrl = data.externalUrl;
        } else {
            delete postData.externalUrl;
        }

		return postData;
    };
});
