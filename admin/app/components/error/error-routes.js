angular.module('l2').config(function($routeProvider) {
	$routeProvider.when('/error', {
		templateUrl: 'app/components/error/error-template.html',
		controller: 'ErrorController'
	});
});