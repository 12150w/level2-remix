angular.module('l2')
.controller('ErrorController', function($scope, $location) {
	if(!$scope.userError) {
		$location.url('/');
	}
});