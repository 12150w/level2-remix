angular.module('l2').config(function($routeProvider) {
	$routeProvider.when('/setup/db', {
		templateUrl: 'app/components/setup/db-template.html',
		controller: 'SetupDBController',
		resolve: {
			setupStatus: function(setup) { return setup.getStatus(); }
		}
	});
	
	$routeProvider.when('/setup/admin', {
		templateUrl: 'app/components/setup/admin-template.html',
		controller: 'SetupAdminController',
		resolve: {
			setupStatus: function(setup) { return setup.getStatus(); }
		}
	});
});