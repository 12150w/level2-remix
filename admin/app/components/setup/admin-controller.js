angular.module('l2')
.controller('SetupAdminController', function($scope, $location, setupStatus) {
	
	// Check if admin setup is already complete
	if(setupStatus === null) {
		$location.url('/setup/db');
	} else if(setupStatus === 'complete') {
		$location.url('/');
	}
	
	$scope.goHome = function() {
		$location.url('/');
	};
});