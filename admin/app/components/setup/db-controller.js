angular.module('l2')
.controller('SetupDBController', function(setupStatus, $location, $scope, api) {
	
	// Check if db setup is already complete
	if(setupStatus === 'complete') {
		$location.url('/');
	} else if(setupStatus === 'db') {
		$location.url('/setup/admin');
	}
	
	// Save database config
	$scope.nextStep = function() {
		$location.url('/setup/admin');
	};
});