angular.module('l2')
.service('setup', function(api) {
	var setup = {};
	
	/* Get Status
	 * 
	 * Returns a promise for the setup status from the /setup/status endpoint
	 *     The promise resolves with the actual status string
	 */
	setup.getStatus = function() {
		return api.get('/setup/status').then(function(res) {
			return res.data.status;
		});
	};
	
	return setup;
});