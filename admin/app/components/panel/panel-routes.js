angular.module('l2').config(function($routeProvider) {
	
	// Panel Home
	$routeProvider.when('/panel', {
		templateUrl: 'app/components/panel/panel.html',
		controller: 'PanelController',
		resolve: {
			panelData: function(api) {
				return api.get('/setup/overview');
			}
		}
	});
	
});