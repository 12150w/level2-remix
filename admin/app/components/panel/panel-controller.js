angular.module('l2')
.controller('PanelController', function(header, panelData, $scope) {
	header.setPageTitle('Admin Panel');

	$scope.panelData = panelData.data;
});
