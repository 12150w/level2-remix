angular.module('l2').config(function($routeProvider) {
	$routeProvider.when('/users/manage', {
		templateUrl: 'app/components/users/manage-template.html',
		controller: 'UsersManageController',
		resolve: {
			userInfo: function(api) {
				return api.get('/users/manage');
			}
		}
	});

	$routeProvider.when('/users/invite', {
		templateUrl: 'app/components/users/invite-template.html',
		controller: 'UsersInviteController'
	});

	$routeProvider.when('/user/:username', {
		templateUrl: 'app/components/users/user-template.html',
		controller: 'UsersUserController',
		resolve: {
			user: function(api, $route) {
				return api.get('/users/details/' + $route.current.params.username).then(function(res) {
					return res.data;
				});
			}
		}
	});

	$routeProvider.when('/invite/accept/:activation_code', {
		templateUrl: 'app/components/users/invite-accept-template.html',
		controller: 'UsersInviteAcceptController',
		resolve: {
			inviteInfo: function(api, $route) {
				return api.get('/users/invite/accept/' + $route.current.params.activation_code).then(function(res) {
					return res.data;
				});
			}
		}
	});
});
