angular.module('l2')
.controller('UsersInviteController', function($scope, $location, header, notification) {
	header.setPageTitle('Send Invitation');
	
	// Success handler
	$scope.onInviteSent = function(res) {
		notification.success('it will be valid until ' + res.data.expiration, 'Invitation Sent');
		$location.url('/panel');
	};
	
});