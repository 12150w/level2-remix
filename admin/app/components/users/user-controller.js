angular.module('l2')
.controller('UsersUserController', function($routeParams, $scope, $location, user, notification) {
	$scope.user = user;
	
	$scope.updateURL = '/users/update/' + user.id;
	
	$scope.backToUsers = function() {
		notification.success('User ' + user.username + ' updated');
		$location.url('/users/manage');
	};
	
	$scope.prepareUpdate = function(data) {
		var postData = {
			activation_status: data.activation_status,
			new_password: data.new_password,
			firstname: data.firstname,
			lastname: data.lastname,
			memberships: []
		};
		
		for(var i=0; i<data.memberships.length; i++) {
			if(data.memberships[i].is_member === true) {
				postData.memberships.push(data.memberships[i].key);
			}
		}
		
		return postData;
	};
});
