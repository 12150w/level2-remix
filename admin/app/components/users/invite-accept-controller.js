angular.module('l2')
.controller('UsersInviteAcceptController', function($scope, $routeParams, inviteInfo) {
    $scope.activationCode = $routeParams.activation_code;
    $scope.acceptUrl = '/users/invite/accept/' + $scope.activationCode;
    $scope.showSuccess = false;

    $scope.inviteData = {
        activationCode: $scope.activationCode,
        username: inviteInfo.username
    };

    $scope.finishInvite = function(data) {
        $scope.showSuccess = true;
        $scope.activationStatus = data.data.activation_status;
    }
});
