angular.module('l2')
.controller('UsersManageController', function($scope, $location, userInfo) {
	$scope.data = userInfo.data;
	
	$scope.openUser = function(user) {
		$location.url('/user/' + user.username);
	};
});