[![build status](https://ci.gitlab.com/projects/4891/status.png?ref=master)](https://ci.gitlab.com/projects/4891?ref=master)

Level2 Remix
=====================================================================================================================
This is the remix version of the level2-api package.
The purpose of this project is to finish the level2-api focusing on the API, and not the framework.
This takes the pieces we love from level2-api and finishes the API.


Installation
---------------------------------------------------------------------------------------------------------------------
To install the level2-remix package in your project just run:

```
npm install --save level2-remix
```


Testing
---------------------------------------------------------------------------------------------------------------------
To run the unit tests for this project use the following command:

```
mocha --recursive tests
```
