API
=====================================================================================================================
The API is the context for an entire API.
It contains stuff such as settings and route handlers.
Any package that uses the level2-remix API will start with an instance of the API class.


Example Usage
---------------------------------------------------------------------------------------------------------------------
Here are a few of the ways to use the API.

### As a Stand-Alone Server
This shows how to start the API using all default settings and a stand-alone Express server.

```javascript
var level2 = require('level2-remix'),
    demoAPI = new level2.API();

// Start the API on port 4200
demoAPI.listen(4200);
```

### As Part of an Existing Express Application
If you already have an Express application and would like to include the API in it:

```javascript
var express = require('express'),
	existingApp = express(),

	demoAPI = new level2.API();

// Start the existing application on port 4200
existingApp.use('/api', demoAPI.router);
existingApp.listen(4200);
```

Note that the API will be accessible under the /api URL instead of the root URL.

### Loading Application Services
If there are services in your application that need to run under the request context
	and access the API use the `API.services()` method to create a router that contains
	these services.
Below is an example of loading services into an API.

```javascript
var app = require('express')(),
	api = new level2.API();

// Load services in the "custom-services" folder
app.use('/custom', api.services("custom-services"));
app.use('/api', api.router);
app.listen();
```

The service being loaded in `custom-services` must export a function as follows:

```javascript
module.exports = function(router, api) {
	// router: the router returned from services()
	// api: the API context
	
	router.get('/some-endpoint', function(req, res) {
		// res.l2 is defined
		
		res.status(200).json({});
	});
}
```


Methods
---------------------------------------------------------------------------------------------------------------------

#### new API([options])
Create a new API instance with the specified options.

* **[options]** `Object`: The options for the API
    * **[settingsPath]** `String`: The path to the API settings (default `'level2-remix-settings.json'`)
    * **[logPath]** `String`: The path to the API log file (default `'level2-log.txt'`)
    * **[dbOptions]** `Object`: Options passed to the Database constructor (default `undefined`)

:arrow_right: Returns a new API instance

#### listen(options...) - `Promise`
Same as nodejs http.Server.listen().
Arguments to this function get passed to express.Application.listen()

:arrow_right: Returns a promise that resolves when the api has started

#### logMessage(message, [source]) - `Promise`
Logs a log message in the API using whatever technique the API uses to store/report logs.

* **message** `String`: The message to log
* **[source]** `String`: Category to sort log messages with

:arrow_right: Returns a promise that resolves when the log is saved

#### logError(message, [source]) - `Promise`
Logs an error in the API using whetever technique the API uses to store/report logs.

* **message** `String`: The error message to log
* **[source]** `String`: Category to sort log messages with

:arrow_right: Returns a promise that resolves when the log is saved

#### services(serviceFolder, api) - `Express.Router`
Returns a router that has mountend the services in the service folder.
Note that services have request context added.

* **serviceFolder** `String`: Path to the folder containing services
	(any file ending in `.js` in this folder will be loaded as a service)
* **api** `API`: The API context these services are running under

:arrow_right: Returns a router with the services mounted

#### getExternalUrl(req) - `Promise - String`
Returns a promise that resolves with the external URL for this api.
The external URL is read from settings if available otherwise the bound
    host and port is used.

* **req** `Express.Request`: The request that this is being made in context of

:warning: The resolved external URL **does not** end with a forward slash `/`

#### exposeCollection(collection)
Adds a collection to be available via the built in data service.
By default **no collections** are exposed.

* **collection** `String`: The name of the collection to expose


Properties
---------------------------------------------------------------------------------------------------------------------

#### router - `Express.Router`
The router used for the API.
User defined routes can be created using this router.

#### settings - `Settings`
The settings for the API (more details about this in the [API Settings Documentation](docs/settings.md)).

#### db - `Database`
The database used for the API.

#### mail - `Mailer`
The Mailer instance created in the context of this API.
