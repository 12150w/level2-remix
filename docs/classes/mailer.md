Mailer
=====================================================================================================================
A Mailer instance allows applications to send emails using the settings stored
    for the API.
All mailer operations take place in the context of an API instance.


Methods
---------------------------------------------------------------------------------------------------------------------

#### new Mailer(api)
Creates a new Mailer instance

* **api** `API`: The API to run this mailer in the context of

:arrow_right: Returns a new Mailer instance

#### send(mailOptions) - `Promise`
Sends an email using the current API settings

* **mailOptions** `Object`: Data pertaining to the email to be sent (passed to Nodemailer)
    * **to** `String|Array`: email address(es) of the recipients (if string separate by `,`)
    * **[text]** `String|Buffer|Stream`: plain text version of the email body
    * **[html]** `String|Buffer|Stream`: html version of the email body
    * **[from]** `String`: the from email address
    * **[sender]** `String`: the sender email address
    * **[cc]** `String|Array`: email address(es) to cc (same format as to)
    * **[bcc]** `String|Array`: email address(es) to bcc (same format as to)
    * **[replyTo]** `String`: email address set as Reply-To value
    * **[subject]** `String`: email subject line
    * **[attachments]** `Array`: array of attachment objects
        * **filename** `String`: filename of attachment
        * **content** `String|Buffer|Stream`: body of the attachment
        * **[contentType]** `String`: content type used, if not set it is derived from filename extension
        * **[cid]** `String`: optional content id used to add in-line images to HTML emails

:arrow_right: Returns a promise that resolves if one of the emails was delivered after all deliveries have
    been attempted

#### on(eventName, handler)
Attaches a handler to a given event


## Events

#### mail

 1. `Object`: the results from Nodemailer
