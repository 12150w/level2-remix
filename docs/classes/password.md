Password
=====================================================================================================================
Encrypts and verifies a password.
Used for storing user's passwords.
Currently the bcrypt encryption method is used to hash passwords.


Instantiating
---------------------------------------------------------------------------------------------------------------------
Instead of calling the usual `new` on the Password class, Passwords must be instantiated using one of the following.

#### Password.fromHash(hashedPassword) - `Password`
Static method that creates a Password from an already encrypted hash.

* **hashedPassword** `String`: The hashed password

:arrow_right: returns the Password instance

#### Password.fromRaw(rawPassword) - `Promise - Password`
Static method that creates a Password from an already encrypted hash

* **rawPassword** `String`: The plain text password that will be encrypted

:arrow_right: returns a promise that resolves with the password instance


Methods
---------------------------------------------------------------------------------------------------------------------

#### matches(comparePassword) - `Promise - Boolean`
Checks that the hashed password matches the compare password.

* **comparePassword** `String`: The raw password to compare the hashed password to

:arrow_right: returns a promise for a boolean that indicates if the passwords matched,
	`true` if matching `false` if they don't


Properties
---------------------------------------------------------------------------------------------------------------------

#### hashed `String`
The hashed password
