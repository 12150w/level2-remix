Database
=====================================================================================================================
Provides access to the API database using promises.
Currently this only supports Postgres via the `pg` module.


Methods
---------------------------------------------------------------------------------------------------------------------

#### new Database(api, [options])
Constructor

* **api** `API`: The API this database is for
* **[options]** `Object`: The database options
    * **[singleClient]** `Boolean`: Set to `true` to use a single Client instead of a Client pool (default `false`)

#### query(sql, [parameters]) - `Promise - Object`
Runs the query and buffers the results.

* **sql** `String`: The SQL of the query to run
* **[parameters]** `Array`: An optional list of parameters to merge into the query

:arrow_right: Returns a promise that resolves with the result object from `pg` (contains a `rows` attribute with the records)

#### upgrade() - `Promise - Object`
Runs any migrations that has not yet been run for this database.

:arrow_right: Returns a promise that resolves when the upgrade is complete.
	Resolves with the updated versions of each table set (`_internal` being the internal table set)

#### changeSchema(schemaName) - `Promise`
Changes the session's default

:arrow_right: Returns a promise that resolves once the schema has changed

#### setMigrationPath(migrationPath)
Sets the external migration path that will be loaded when the API starts.

* **migrationPath** `String`: The path to the folder containing the external migrations


Properties
---------------------------------------------------------------------------------------------------------------------

#### store - `DataStore`
A DataStore instance under the context of this API.
This is used by the data API to provide its functionality and may be
used by custom applications to access the database as if via the data API.
