TestAPI - API
=====================================================================================================================
Runs a test API for unit testing under a socket.


Methods
---------------------------------------------------------------------------------------------------------------------

#### new TestAPI([options])
Creates a new TestAPI instance.

* **[options]** `Object`: The options for the new TestAPI
    * **[doSetup]** `Boolean`: Set to `true` to do server setup automatically (default `false`)
    * **[logPath]** `String`: Sets the log path of the test API


#### listen() - `Promise`
Starts the test API under a unix socket.


#### stop() - `Promise`
Stops the test API and cleans up;


#### request(options) - `Promise - Object`
Sends a request to this API.

* **options** `Object`: The options passed to the request library

:arrow_right: Returns a promise that resolves with the request's result (the result being the same as the promise version of `request()`)


#### secureRequest(options) - `Promise - Object`
Sends a request with a valid authorization header

* **options** `Object`: Options passed to the request library

:arrow_right: Returns a promise that resolves with the request results


#### sessionRequest(options) - `Promise - Object`
Sends a request with a valid user session in the headers

* **options** `Object`: Options passed to the request library

:arrow_right: Returns a promise that resolves with the request results


#### get(url) - `Promise - Object`
Sends a GET request to the url.

* **url** `String`: The url to GET

:arrow_right: Returns a promise for the response


#### secureGet(url) - `Promise - Object`
Sends a GET request with a valid admin SID

* **url** `String`: The url to GET

:arrow_right: Returns a promise for the response


#### sessionGet(url) - `Promise - Object`
Sends a GET request with a valid user session in the headers

* **url** `String`: The url to GET

:arrow_right: Returns a promise for the response


#### post(url, data) - `Promise - Object`
Sends a POST request to the url with data

* **url** `String`: The url to POST to
* **data** `Object`: The data to be sent as JSON

:arrow_right: Returns a promise for the response


#### securePost(url, data) - `Promise - Object`
Sends a POST request with valid admin authorization

* **url** `String`: The url to POST to
* **data** `Object`: The data to be sent as JSON

:arrow_right: Returns a promise for the response


#### sessionPost(url, data) - `Promise - Object`
Sends a POST request with a valid user session in the headers

* **url** `String`: The url to POST to
* **data** `Object`: The data to be sent as JSON

:arrow_right: Returns a promise for the response


#### sessionPut(url, data) - `Promise - Object`
Sends a PUT request with a valid user session in the headers

* **url** `String`: The url to PUT to
* **data** `Object`: The data to be sent as JSON

:arrow_right: Returns a promise for the response


#### sessionDelete(url, data) - `Promise - Object`
Sends a DELETE request with a valid user session in the headers

* **url** `String`: The url to DELETE to
* **data** `Object`: The data to be sent as JSON

:arrow_right: Returns a promise for the response


#### getTestSession() - `Promise - String`
Gets a valid SID for a valid user

:arrow_right: Returns a promise for the valid SID
