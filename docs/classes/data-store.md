DataStore
=====================================================================================================================
Provides an interface to database collections so that data may be accessed as though it were being accessed via the
	data API.
Since the methods mirror the data API the tests for this class are actually the data API tests.



Methods
---------------------------------------------------------------------------------------------------------------------


#### new DataStore(db)

* **db** `Database`: the database instance to back this data store


#### create(collection, data) - `Promise - Object`
Inserts a new record into the collection

* **collection** `String`: the name of the collection to insert the record into
* **data** `Object`: the fields and value to be set on the new record

:arrow_right: Returns a promise for the following object

* **id** `String|Number`: the identity of the newly inserted record
* **data** `Object`: all the fields of the new record


#### findById(collection, ids) - `Promise - Array`
Finds the records in a collection that have matching ids

* **collection** `String`: the name of the collection to query
* **ids** `Array`: list of ids of records to return

:arrow_right: Returns a promise for the following object

* **records** `Array`: list of records with matching ids
    * **id** `String|Number`: the identity of the record
	* **data** `Object`: the fields and values of the record


#### query(collection, query) - `Promise - Object`
Runs a query in the collection

* **collection** `String`: the collection to run the query in
* **query** `Object`: the query object describing the query
	(see the [Data Service Documentation](docs/services/data.md) for details)

:arrow_right: Returns a promise for the query results with the following

* **records** `Array`: list of records with matching ids
    * **id** `String|Number`: the identity of the record
	* **data** `Object`: the fields and values of the record


#### update(collection, id, data) - `Promise - Object`
Updates a single record in a collection matching on id with the given data.

* **collection** `String`: the collection name that contains the record to update
* **id** `String|Number`: the identity of the record to update
* **data** `Object`: key value pairs of the fields to update

:arrow_right: Returns a promise that includes the updated record in the following format

* **id** `String|Number`: the identity of the updated record
* **data** `Object`: the fields and values of the updated record


#### delete(collection, id) - `Promise`
Deletes a record from the collection

* **collection** `String`: the collection that the record is deleted from
* **id** `String|Number`: the id of the record to delete

:arrow_right: returns a promise that resolves when the record is deleted
