base.Class
=====================================================================================================================
The base Class implementation used across the project.
You can make a new sub-class definition by using `extend()`.
For example to make a `Car` class with a `drive()` method and `color` attribute we could write:

```javascript
var level2 = require('level2-remix');

var Car = base.Class.extend({
    init: function(color) {
        this.color = color;
    },
    
    drive: function(distance) {
        // Add driving functionality here
    }
});

var redCar = new Car('red');
var blueCar = new Car('blue');
```

From there we could extend the car to define some sub class.
Say we wanted a rusty car that extended the car, we would write:

```javascript
var RustyCar = Car.extend({
    init: function() {
        this._super('brown');
    },
    
    drive: function(distance) {
        if(distance > 100) {
            throw new Exception('A RustyCar can not driver over 100 miles');
        } else {
            this._super(distance);
        }
    }
});
```

Notice that in overridden methods the special `this._super()` is defined to access the parent's method.


Methods
---------------------------------------------------------------------------------------------------------------------

#### extend([mixins...], definition) - `Function`
Extends the class that this is called on.

* **definition** `Object`: the new extended class definition
* **[mixins...]**: optional list of Class extensions to be used as mixins for this class definition

:arrow_right: Returns the extended child class
