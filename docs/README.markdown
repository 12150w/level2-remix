Level2 Remix Documentation
=====================================================================================================================
You have reached the documentation for the level2-remix package.
Information here will guide you through using the level2-remix API.
We also a some nifty API documentation.


Service Documentation
---------------------------------------------------------------------------------------------------------------------
This has the detailed information about HTTP endpoints exposed by the API.

#### [Admin Service](docs/services/admin.md)

#### [Email Service](docs/services/email.md)

#### [Root Service](docs/services/root.md)

#### [Setup Service](docs/services/setup.md)

#### [Users Service](docs/services/users.md)

#### [Session Service](docs/services/session.md)

#### [Data Service](docs/services/data.md)

#### [Account Service](docs/services/account.md)


API Documentation
---------------------------------------------------------------------------------------------------------------------
All the classes defined here are accessible via the main package script.

### Classes

#### [API](docs/classes/api.md)
The context for the entire API, this is where it starts

#### [Database](docs/classes/db.md)
Interface to the database in the context of the API

#### [DataStore](docs/classes/data-store.md)
CRUD API mirroring the Data Service

#### [Mailer](docs/classes/mailer.md)
Interface to send emails in the context of the API

#### [Password](docs/classes/password.md)
Encrypts and verifies a password

#### [TestAPI](docs/classes/test-api.md)
An API used for unit testing, runs under a socket

#### [base.Class](docs/classes/class.md)
A base class implementation, making it easy to extend


Various Documentation
---------------------------------------------------------------------------------------------------------------------

#### [API Settings](docs/settings.md)

#### [Express Request Context](docs/request-context.md)
