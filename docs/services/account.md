# Account Service
Allows users to manage their account.


## POST `/account/prr/request` :unlock:
Creates a new password reset request (PRR) for a certain user.

#### Request

* **username** `String`: The username of the user to reset the password of

:warning: Only 1 active (unused, non-expired, non-corrupt) PRR can be created.
	If there is already 1 active PRR a 400 response will be returned.


## POST `/account/prr/execute` :unlock:
Resets a user's password given their active PRR code.

#### Request

* **code** `String`: The PRR code that was emailed to the user
* **password** `String`: The new password to use for the user

:warning: If the PRR code is invalid a 400 response will be returned.
