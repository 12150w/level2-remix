Session Service
=====================================================================================================================
Handles user sessions including login and logout.


POST `/session/login` :unlock:
---------------------------------------------------------------------------------------------------------------------
Logs in a user and creates a new session.
If an active session is alread found then the session timeout is updated and the same session is re-used.

#### Request

* **username** `String`: The user's username
* **password** `String`: The user's password

#### Response

* **sid** `String`: The valid SID


POST `/session/logout` :lock:
---------------------------------------------------------------------------------------------------------------------
Closes a session therefore logging the user out.
If the session is already closed then this endpoint does nothing *but it does not fail*.
The SID is loaded from the normal `Authorization` header or cookie.
On success the endpoint will clear any authorization cookies.
