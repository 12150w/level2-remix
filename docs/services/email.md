Email Service
=====================================================================================================================
Provides functionality to set email settings.


GET `/email/settings` :key:
---------------------------------------------------------------------------------------------------------------------
Returns the current email settings

#### Response

* **transport** `String`: the transport module named use to send emails
* **options** `Mixed`: the options passed to the transport when being constructed
* **defaultFrom** `String`: the default from address
* **externalUrl** `String`: the external link URL


POST `/email/settings` :key:
---------------------------------------------------------------------------------------------------------------------
Updates the email settings

#### Request

* **transport** `String`: the transport to use, valid values are (`'debug'`, `'smtp'`, or `'direct'`)
* **options** `Mixed`: data to be passed to the transport when being created
* **defaultFrom** `String`: the default from address for emails sent from the server
* **externalUrl** `String`: the base URL used for links in emails
