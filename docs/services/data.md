Data Service
=====================================================================================================================
The data service provides generic access to database tables that are exposed via the API (some internal tables are
	not exposed for security reasons).
To help document this service more clearly the below document is broken up into sections by functionality.

By default **NO COLLECTIONS ARE EXPOSED** by this API.
In order to expose a collection by this API the `API.exposeCollection()` method must be called with the
	collection name when starting the API.
For example to expose a table named `contacts` the following would be used.

```javascript
api.exposeCollection('contacts');
```


Creating Records :lock:
---------------------------------------------------------------------------------------------------------------------
To create a record that is part of a collection (table) then a POST request must be made with the following data:

* **data** `Object`: the fields and values of the new record

The URL that this data is sent to has the form `/data/{collection}` where collection is the name of the table the
	data should be inserted into.
In response to the create the new record, with *all* fields on the record, are returned as follows:

* **id** `Number|String`: the identifier for the created record
* **data** `Object`: all fields and values of the inserted record (including auto generated ones)

### Examples

#### Creating a Contact
Say we had a simple `contacts` table with fields for name.
To insert a new contact a POST would be made to `/data/contacts` with the following body:

```json
{
    "first_name": "Joe",
    "last_name": "Smith"
}
```

Now assume we had an auto-generated id and a auto generated customer code, both values would be returned.

```json
{
	"id": 123,
	"data": {
		"first_name": "Joe",
		"last_name": "Smith",
		"customer_code": "JS-A2T9"
	}
}
```


Finding Records :lock:
---------------------------------------------------------------------------------------------------------------------
A record may be loaded 1 of 2 ways: by an id or by a query.

To find a record by its identity send a GET request to the URL `/data/{collection}/{id}`.
The response will be formatted as follows:

```json
{
    "id": 123,
	"data": {
		"first_name": "Joe",
		"last_name": "Smith",
		"customer_code": "JS-A2T9"
	}
}
```

If no matching record is found then an empty object (`{}`) is returned.

The other way to find records is by a query.
Queries are sent as the body of a POST request as a JSON encoded object.
A query can contain complex operations by using operators starting with `$`.
Any operator not starting with `$` are considered fields and are evaluated as equal filters.

### EQUAL Operator

```json
{
	"<FIELD>": "<VALUE>"
}
```

### IN Operator

```json
{
    "<FIELD>": [1, 2, 3]
}
```

To make a query the query must be in the body of a POST to the URL `/data/query/{collection}`.
Data is returned in the following format:

```json
{
	"records": [
		{
			"id": 1,
			"data": {}
		},
		{
			"id": 2,
			"data": {}
		}
	]
}
```


Updating Records :lock:
---------------------------------------------------------------------------------------------------------------------
A record may be updated by sending a PUT request to the URL `/data/{collection}/{id}`.
The body of the PUT request must contain the field value pairs to be updated.
Only the fields included in the body will be updated.

### Examples

#### Updating a Contact
To update a contact with id `1` send a PUT request to `/data/contacts/1` with the following data:

```json
{
	"name": "New Name"
}
```

This would cause the following response to be returned:

```json
{
	"id": 1,
	"data": {
		"name": "New Name",
		"some_other_field": "Other Value"
	}
}
```

Notice that the response contains all the fields on the contact, not just the ones that were updated.


Deleting Records :lock:
---------------------------------------------------------------------------------------------------------------------
To delete a record a DELETE request is sent to the URL with `/data/{collection}/{id}`

### Examples

#### Deleting a Contact
To delete the contact with id `1` a DELETE request would be made to `/data/contacts/1`.
