Setup Service
=====================================================================================================================
Endpoints to setup the server.


POST `/setup/database` :unlock:
---------------------------------------------------------------------------------------------------------------------
Sets up access to the Postgres database

#### Request

* **database** `String`: The name of the database to use
* **[host]** `String`: Host name of the database server
* **[port]** `Number`: Port number to use when connecting to the database
* **[user]** `String`: Username used to connect to database
* **[password]** `String`: Password used to connect to database

:warning: Only accessible if setup is **not** complete


POST `/setup/admin` :unlock:
---------------------------------------------------------------------------------------------------------------------
Saves the initial server configuration

#### Request

* **username** `String`: New admin username
* **email** `String`: New admin email
* **password** `String`: New admin password

:warning: Only accessible if setup is **not** complete


GET `/setup/status` :unlock:
---------------------------------------------------------------------------------------------------------------------
Displays the current setup status

#### Response

* **status** `String`: Indicates what step the setup is at, either `null`, `'db'`, or `'complete'`


GET `/setup/overview` :key:
---------------------------------------------------------------------------------------------------------------------
Lists the overall settings, used on the admin panel

#### Response

* **admin** `Object`: The admin user information
    * **username** `String`: Admin username
    * **email** `String`: Admin email
* **users** `Object`: The user information
    * **count** `Number`: The total number of users
* **db** `Object`: Contains an overview of the database settings
	* **internalIndex** `Number`: The migration index of the internal migrations
	* **externalIndex** `Number`: The migration index of the external migrations
