Admin Service
=====================================================================================================================
General admin interface functionality (such as admin login).


POST `/admin-api/login` :unlock:
---------------------------------------------------------------------------------------------------------------------
Creates a new session as the admin user.

#### Request

* **username** `String`: The admin's username
* **password** `String`: The admin's password

#### Response

* **sid** `String`: The valid session id for the admin user

:information_source: If an existing valid session is found for the admin user
	that session id is used (and the session expiration is updated)
