Users Service
=====================================================================================================================
Manages creating and modifying users.


POST `/users/invite` :key:
---------------------------------------------------------------------------------------------------------------------
Adds a new user if accessed with admin permissions.

#### Request

* **username** `String`: The username of the new user to create
* **email** `String`: Email of the new user to create

#### Response

* **activationCode** `String`: The activation code sent in the invitation email
* **expiration** `String`: Date and time that the invitation expires


GET `/users/manage` :key:
---------------------------------------------------------------------------------------------------------------------
Retrieves information about the current users

#### Response

* **users** `Array - Object`: The users that are part of this API
    * **id** `Number`: The user's unique Id
    * **username** `String`: The user's username
    * **email** `String`: The user's email
    * **activation_status** `String`: If null indicates that the user is active, otherwise contains reason


GET `/users/details/:username` :key:
---------------------------------------------------------------------------------------------------------------------
Retrieves the information about the user defined by the username in the URL

#### Response

* **id** `Number`: The user's unique id
* **username** `String`: The user's username
* **email** `String`: The user's username
* **activation_status** `String`: The user's current activation status
* **memberships** `Array - Object`: List of all groups
	* **key** `String`: The group's key
	* **is_member** `Boolean`: Indicates whether the user is a member of the group (`true` if member, otherwise `false`)


POST `/users/update/:user_id` :key:
---------------------------------------------------------------------------------------------------------------------
Updates the user information

#### Request

* **activation_status** `String`: The user's activation status (if not provided activation_status will be cleared)
* **[new_password]** `String`: update the user's password to a new password
	(if not specified the password will not change)
* **[memberships]** `Array - String`: group keys that the user is a member of
	(if a membership exists but is not part of this array, *it will be deleted*)
* **[firstname]** `String`: an updated firstname (if not provided firstname will not change)
* **[lastname]** `String`: an updated lastname (if not provided lastname will not change)


GET `/users/invite/accept/:activation_code` :unlock:
---------------------------------------------------------------------------------------------------------------------
Returns the details about the invitation status

#### Response

* **username** `String`: the username associated with this invitation


POST `/users/invite/accept/:activation_code` :unlock:
---------------------------------------------------------------------------------------------------------------------
Accept an invitation if it is valid

#### Request

* **password** `String`: The user's new password

#### Response

* **username** `String`: The activated user's username
* **activation_status** `String`: The user's currenct activation_status


GET `/users/all-groups` :key:
---------------------------------------------------------------------------------------------------------------------
Returns a list of all the groups

#### Response

* **groups** `Array - Object`: A list of all the group records
	* **key** `String`: The groups key (serves as the id)


POST `/users/create-group` :key:
---------------------------------------------------------------------------------------------------------------------
Creates a new group

#### Request

* **key** `String`: The key of the group to create


POST `/users/delete-group` :key:
---------------------------------------------------------------------------------------------------------------------
Deletes a group

#### Request

* **key** `String`: The key of the group to delete
