API Settings
=====================================================================================================================
Each API instance contains a set of settings.
These settings are documented here.

* **setup**: Settings pertaining to the setup of the server
    * **complete**: Set to `true` once the server setup is complete (set to `false` to re-do setup)
    * **internalMigrationIndex**: A number indicating the index of the last executed migration
    * **admin**: The admin user information
        * **username**: The admin username
        * **password**: The encrypted admin password
        * **email**: The admin recovery email
* **postgres**: Postgres database configuration
    * **database**: The name of the database used by the API
    * **[host]**: The database server host name
    * **[port]**: The database server port
    * **[user]**: The user used to connect to postgres
    * **[password]**: The password used to connect to postgres
* **session**: The admin user's session
    * **sid**: The current session id
    * **validUntil**: The date and time that this admin session is valid until
* **users**: User configuration
    * **defaultStatus**: The activation status of users after they have accepted an invitation
        (if `null` or `undefined` then users are active after accepting invitations)
* **email**: Settings for sending emails from the server
    * **transport**: The mail transport mechanism used
    * **options**: Data passed to the transport during creation
	* **defaultFrom**: The email that is used in the from address in emails sent by default
* **externalMigrationIndex**: The external migration index for user loaded migrations
* **externalUrl**: The base of external url used for external links (such as email links)


## Environment Overrides
In order to support deployment on Kubernetes the following environment variables can be defined.
If defined, an environment variable **overrides** the settings file.

* `L2_PG_HOST`: overrides `postgres.host`
* `L2_PG_DB`: overrides `postgres.database`
* `L2_PG_USER`: overrides `postgres.user`
* `L2_PG_PW`: overrides `postgres.password`
