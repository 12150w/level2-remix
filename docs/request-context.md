Request Context (`res.l2`)
=====================================================================================================================
All responses send through the API have the `l2` object added to the response.
This object contains information about the request pertaining to the API.
That is to say the API request context is stored in the `l2` attribute on the response.


Methods
---------------------------------------------------------------------------------------------------------------------

#### res.l2.sendError(message, httpStatus, [errorCode], [extraAttributes])
Sends an error in response to the request using the following format:

```json
{
    "msg": "<message argument>",
    "code": "<errorCode argument>"
}
```

* **message** `String`: The error message
* **httpStatus** `Number`: The HTTP status to use in the response
* **[errorCode]** `String`: An optional identifier for this type of error
* **[extraAttributes]** `Object`: Optional attributes to merge into the response body

The valid error codes are

| Code                      | Description                                                                       |
|---------------------------|-----------------------------------------------------------------------------------|
| `INVALID_CLIENT_CODE`     | The endpoint indicated an invalid client code which is stored in `invalidCode`    |
| `REDIRECT`                | Indicates that the user should redirect to the `url` in the error body            |
| `EXCEPTION`               | The endpoint code threw an error, likely in a promise (contains `stack`)          |
| `NOT_SETUP`               | The initial server setup must be compmleted                                       |
| `INVALID_ENDPOINT`        | The URL did not match any request endpoint handler                                |
| `NO_LOGIN`                | The user is not logged in and should be redirected to the login page              |


#### res.l2.catchError(err)
Catches a JavaScript error and sends it in response to the request.

* **err** `Error`: The error to send back


#### res.l2.validateAdmin() - `Promise`
Verifies that the request has a valid **admin** SID included.

:arrow_right: Returns a promise that resolves if the request was made with a valid **admin** SID, otherwise the promise fails


#### res.l2.validateUser([groups]) - `Promise - Number`
Verifies that the request has a valid **user** SID included, if not the promise fails.
If groups are specified then the user also has to be a member of at least one of the
	specified groups.

An SID may be specified by either storing it in the `Authorization` header or in the `l2sid` cookie.
The `Authorization` header value will be used before the `l2sid` cookie value.

* **[groups]** `String | Array - String`: Group key or list of group keys that the user must be a member of at least one

:arrow_right: Returns a promise that resolves if the request was made with a valid SID and if the user is
	a member of at least one of the specified groups (if any are specified).
	The promise resolves with the session user id.

Properties
---------------------------------------------------------------------------------------------------------------------

#### res.l2.isSetupRoute - `Boolean`
`true` if this request is to be handled by the Setup Service, otherwise `false`.
