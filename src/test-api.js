var API = require('./api'),
	promiseRequest = require('./request'),
	testUtil = require('./test-util'),
	os = require('os'),
	crypto = require('crypto'),
	path = require('path'),
	q = require('q'),
	fs = require('fs');

var TestAPI = API.extend({
	init: function(testOptions) {
		testOptions = testOptions || {};
		this._hasCreatedSchema = false;
		this._hasCreatedAdmin = false;
		this.path = null;

		// Generate a testing schema name
		this._apiNumber = ++TestAPI._testAPICount;
		this._schemaName = 'test_api_' + this._apiNumber;

		testOptions.settingsPath = path.join(os.tmpdir(), 'test-api-' + this._apiNumber + '.json');
		testOptions.logPath = testOptions.logPath || path.join(os.tmpdir(), 'test-log-' + this._apiNumber + '.txt');
		testOptions.doSetup = testOptions.doSetup === true ? true : false;
		testOptions.dbOptions = {singleClient: true};

		this._validSID = null;

		TestAPI._instances.push(this);
		var superReturn = this._super(testOptions);

		// Disable auto update
		this._autoUpdate = false;

		return superReturn;
	},

	listen: function() {
		var self = this,
			apiListen = self._super.bind(self);

		return q.nfcall(crypto.pseudoRandomBytes, 3).then(function(rawKey) {
			if(self.path != null) return;

			// Come up with a valid unix socket or named pipe
			var socketPath;
			if(os.platform() === 'win32') socketPath = '\\\\?\\pipe';
			else socketPath = os.tmpdir();
			socketPath = path.join(socketPath, 'level2-remix-' + rawKey.toString('hex').toUpperCase() + '.sock');
			self.path = socketPath;
			return apiListen(socketPath);
		}).then(function() {
			// Set up the database
			if(self._hasCreatedSchema) return;

			var testDBConfig = {
				database: 'level2_remix',
				host: '127.0.0.1',
				user: 'level2_remix',
				password: 'level2_remix'
			};
			return self.settings.set('postgres', testDBConfig).then(function() {
				return self.db.query('CREATE SCHEMA ' + self._schemaName);
			}).then(function() {
				self._hasCreatedSchema = true;
			}).then(function() {
				return self.db.changeSchema(self._schemaName);
			});

		}).then(function() {
			// Upgrade the database to the lates
			return self.db.upgrade();

		}).then(function() {
			if(self._options.doSetup !== true || self._hasCreatedAdmin === true) return;

			// Set up the admin user
			return self.request({
				url: '/setup/admin',
				method: 'POST',
				json: true,
				body: {
					username: 'test',
					password: 'test',
					email: 'unit.test@12150w.com'
				}
			}).then(function() {
				// Disable doing admin setup any more
				self._hasCreatedAdmin = true;
			});
		});
	},

	stop: function() {
		var self = this;

		// Delete the settings file
		return q.ninvoke(fs, 'unlink', self._useSettingsPath).fail(function(err) {
			if(err != null && err.code !== 'ENOENT') {
				throw err;
			}

		}).then(function() {
			// Delete the log file
			return q.ninvoke(fs, 'unlink', self._logPath).fail(function(err) {
				if(err != null && err.code !== 'ENOENT') {
					throw err;
				}
			});

		}).then(function() {
			// Stop the server
			if(self._netServer == null) return;
			return q.ninvoke(self._netServer, 'close');

		}).then(function() {
			// Delete the testing schema
			if(self._hasCreatedSchema !== true) return;
			return self.db.query('DROP SCHEMA ' + self._schemaName + ' CASCADE').fail(function(err) {
				console.log(err.stack);
			});

		});
	},

	request: function(options) {
		options.baseUrl = 'http://unix:' + this.path + ':/';
		return promiseRequest(options);
	},

	secureRequest: function(options) {
		var self = this;

		return self._getAdminSID().then(function(adminSID) {
			options.headers = options.headers || {};
			options.headers['Authorization'] = adminSID;
			return self.request(options);
		});
	},

	sessionRequest: function(options) {
		var self = this;

		return self.getTestSession().then(function(sid) {
			options.headers = options || {};
			options.headers['Authorization'] = sid;
			return self.request(options);
		});
	},

	get: function(url) {
		return this.request({
			uri: url,
			method: 'GET',
			json: true
		});
	},

	secureGet: function(url) {
		return this.secureRequest({
			uri: url,
			method: 'GET',
			json: true
		});
	},

	sessionGet: function(url) {
		return this.sessionRequest({
			url: url,
			method: 'GET',
			json: true
		});
	},

	post: function(url, data) {
		return this.request({
			uri: url,
			method: 'POST',
			json: true,
			body: data
		});
	},

	securePost: function(url, data) {
		return this.secureRequest({
			uri: url,
			method: 'POST',
			json: true,
			body: data
		});
	},

	sessionPost: function(url, data) {
		return this.sessionRequest({
			url: url,
			method: 'POST',
			json: true,
			body: data
		});
	},

	sessionPut: function(url, data) {
		return this.sessionRequest({
			url: url,
			method: 'PUT',
			json: true,
			body: data
		});
	},

	sessionDelete: function(url, data) {
		return this.sessionRequest({
			url: url,
			method: 'DELETE',
			json: true,
			body: data
		});
	},

	getTestSession: function() {
		var self = this;
		if(self._validSession != null) return q (self._validSession);

		// Invite the user
		return self.securePost('/users/invite', {
			username: 'unit-test',
			email: testUtil.getUniqueEmail()
		}).then(function(res) {
			// Accept the invitation
			return self.post('/users/invite/accept/' + res.body.activationCode, {
				password: 'test'
			});

		}).then(function(res) {
			// Verify the user is active
			return self.db.query(
				'UPDATE l2_users SET activation_status = $1 WHERE username = $2',
				[null, 'unit-test']
			);

		}).then(function(res) {
			// Login the user
			return self.post('/session/login', {
				username: 'unit-test',
				password: 'test'
			});

		}).then(function(res) {
			// Record the SID
			self._validSession = res.body.sid;
			return self._validSession;

		});
	},

	_getAdminSID: function() {
		// Returns a promise for the valid admin SID
		var self = this;
		if(self._validSID !== null) return q(self._validSID);

		return self.request({
			url: '/admin-api/login',
			method: 'POST',
			json: true,
			body: {
				username: 'test',
				password: 'test'
			}
		}).then(function(result) {
			self._validSID = result.body.sid;
			return self._validSID;
		});
	}
});

TestAPI._testAPICount = 0;
TestAPI._instances = [];

// Add the mocha after handler if available
if(typeof(after) === 'function') {
	after(function(done) {
		var api,
			stopPromises = []

		for(var _i=0; _i<TestAPI._instances.length; _i++) {
			api = TestAPI._instances[_i];
			stopPromises.push(api.stop());
		}

		q.all(stopPromises).then(function() {
			done()
		}, done).done();
	});
};

module.exports = TestAPI;
