/*
 * Data Service
 *    Provides generic access to database collections
 *
 */
var ResponseError = require('../errors').ResponseError;

module.exports = function(api) {

	// Check for restricted collections
	var validateCollection = function(collection) {
		if(api._exposedCollections[collection] !== true) {
			throw new ResponseError('Restricted collections cannot be accessed', 400);
		}
	};

	// Finding Records by Id
	api.router.get('/data/:collection/:id', function(req, res) {
		res.l2.validateUser().then(function() {
			// Find the record
			validateCollection(req.params.collection);
			return api.db.store.findById(req.params.collection, [parseInt(req.params.id, 10)]);

		}).then(function(findRes) {
			// Respond
			if(findRes.records.length < 1) return res.status(200).json({});
			var record = findRes.records[0];
			res.status(200).json(record);

		}).fail(res.l2.catchError);
	});


	// Query
	api.router.post('/data/query/:collection', function(req, res) {
		res.l2.validateUser().then(function() {
			// Run the query
			validateCollection(req.params.collection);
			return api.db.store.query(req.params.collection, req.body);

		}).then(function(queryRes) {
			//Respond
			res.status(200).json(queryRes);

		}).fail(res.l2.catchError);
	});


	// Record Creation
	api.router.post('/data/:collection', function(req, res) {
		res.l2.validateUser().then(function() {
			// Run the insert
			validateCollection(req.params.collection);
			return api.db.store.create(req.params.collection, req.body);

		}).then(function(createRes) {
			// Respond
			res.status(200).json(createRes);

		}).fail(res.l2.catchError);
	});


	// Record Update
	api.router.put('/data/:collection/:id', function(req, res) {
		res.l2.validateUser().then(function() {
			// Run the update
			validateCollection(req.params.collection);
			return api.db.store.update(req.params.collection, parseInt(req.params.id, 10), req.body);

		}).then(function(updateRes) {
			// Respond
			res.status(200).json(updateRes);

		}).fail(res.l2.catchError);
	});


	// Record Deletion
	api.router.delete('/data/:collection/:id', function(req, res) {
		res.l2.validateUser().then(function() {
			// Run the delete
			validateCollection(req.params.collection);
			return api.db.store.delete(req.params.collection, parseInt(req.params.id, 10));

		}).then(function(deleteRes) {
			// Respond
			res.status(200).json({});

		}).fail(res.l2.catchError);
	});

};
