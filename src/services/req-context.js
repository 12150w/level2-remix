/* 
 * Request Context
 *    Attaches the request context handlers to an app or router
 *
 */
var q = require('q'),
	errors = require('../errors'),
	cookieParser = require('cookie-parser'),
	sessionServices = require('./session'),
	ResponseError = errors.ResponseError;

 // All allowed error codes
 var VALID_ERROR_CODES = [
 	'INVALID_CLIENT_CODE',
 	'REDIRECT',
 	'EXCEPTION',
 	'NOT_SETUP',
 	'INVALID_ENDPOINT',
 	'NO_LOGIN'
 ];

module.exports = function(router, api) {
	router.use(cookieParser());
	router.use(function(req, res, next) {
		res.l2 = {};

		res.l2.sendError = _sendError_.bind(res);
		res.l2.catchError = _catchError_.bind(res);
		res.l2.validateAdmin = _validateAdmin_.bind({req: req, api: api});
		res.l2.validateUser = _validateUser_.bind({req: req, api: api});
		res.l2.isSetupRoute = false;
		
		res.set('X-Powered-By', 'level2-remix');
		res.set('Content-Type', 'application/json; charset=utf-8');

		next();
	});
};

// Send Error
var _sendError_ = function(message, httpStatus, errorCode, extraAttributes) {
	// Sends an error in response to a request
	// NOTE: this is the response object
	extraAttributes = extraAttributes || {};

	// Validate error code
	errorCode = errorCode || null;
	if(errorCode !== null && VALID_ERROR_CODES.indexOf(errorCode) < 0) {
		extraAttributes.invalidCode = errorCode;
		errorCode = VALID_ERROR_CODES[0];
	}

	// Build the body
	var errorBody = {
		msg: message
	};
	if(errorCode !== null) errorBody.code = errorCode;
	for(var key in extraAttributes) {
		errorBody[key] = extraAttributes[key];
	}

	// Send the response
	this.status(httpStatus).json(errorBody);
};

// Catch Error
var _catchError_ = function(err) {
	// Sends a standard JavaScript error in response

	// Check for ResponseError
	if(err instanceof errors.ResponseError) {
		return this.l2.sendError(
			err.message,
			err.httpStatus,
			err.errorCode,
			err.extraAttributes
		);
	}

	this.l2.sendError(String(err), 500, 'EXCEPTION', {stack: err.stack});
};

// Validate Admin
var _validateAdmin_ = function() {
	var self = this;

	// Returns a promise that fails if this in not a valid admin session
	return self.api.settings.get('session').then(function(session) {

		// Validate session exists
		if(!session) throw new errors.ResponseError('You do not have permission to access this resource', 401, 'NO_LOGIN');

		// Validate request SID exists
		var reqSID = self.req.get('Authorization');
		if(!reqSID) throw new errors.ResponseError('No session Id was included in request', 401, 'NO_LOGIN');

		// Validate session not expired
		var untilNumber = session.validUntil;
		if(typeof(untilNumber) !== 'number') throw new errors.ResponseError('You do not have permission to access this resource', 401, 'NO_LOGIN');
		var validUntil = new Date(untilNumber),
			now = new Date();
		if(validUntil < now) throw new errors.ResponseError('You do not have permission to access this resource', 401, 'NO_LOGIN');

		// Validate SID
		if(reqSID !== session.sid) throw new errors.ResponseError('You do not have permission to access this resource', 401, 'NO_LOGIN');
	});
};

// Validate User
var loadSessionQuery = 'SELECT s.sid, s.expires, s.user_id, m.group_key ';
loadSessionQuery +=    'FROM l2_sessions AS s ';
loadSessionQuery +=    'LEFT OUTER JOIN l2_memberships AS m ON s.user_id = m.user_id ';
loadSessionQuery +=    'WHERE s.sid = $1 AND LOCALTIMESTAMP < s.expires;';
var _validateUser_ = function(groups) {
	var self = this;
	var session;

	var checkGroups = null;
	if(!!groups) {
		if(Object.prototype.toString.call(groups) !== '[object Array]') {
			checkGroups = [groups];
		} else {
			checkGroups = groups;
		}
	}

	return q.fcall(function() {
		// Load the SID and session
		var sid = self.req.get('Authorization') || self.req.cookies['l2sid'];
		if(!sid) throw new ResponseError('No authorization information found in request', 401, 'NO_LOGIN');
		return self.api.db.query(loadSessionQuery, [sid]);

	}).then(function(sessionRes) {
		// Group by groups
		if(sessionRes.rows.length < 1) throw new ResponseError('Invalid SID', 401);
		session = {
			sid: sessionRes.rows[0].sid,
			expires: sessionRes.rows[0].expires,
			userId: sessionRes.rows[0].user_id,
			groups: []
		};

		for(var i=0; i<sessionRes.rows.length; i++) {
			session.groups.push(sessionRes.rows[i].group_key);
		}

	}).then(function() {
		// Validate the group membership
		if(checkGroups === null) return;
		var inOne = false;

		for(var i=0; i<checkGroups.length; i++) {
			if(session.groups.indexOf(checkGroups[i]) > -1) {
				inOne = true;
				break;
			}
		}
		if(inOne === false) throw new ResponseError('You do not have permission to access this resource', 401);

	}).then(function() {
		// Resolve with the user id
		return session.userId;

	});
};
module.exports._validateUser_ = _validateUser_;
