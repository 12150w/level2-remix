/*
 * Session Service
 *
 */
var q = require('q'),
	ResponseError = require('../errors').ResponseError,
	password = require('../password'),
	crypto = require('crypto');

var SESSION_TIMEOUT = '3 hours';

module.exports = function(api) {

	// Login
	api.router.post('/session/login', function(req, res) {
		var reqUsername, reqPassword,
			userId = null;

		return q.fcall(function() {
			// Validate the body
			if(!req.body.username) throw new ResponseError('Username is required', 400);
			if(!req.body.password) throw new ResponseError('Password is required', 400);
			reqUsername = req.body.username;
			reqPassword = req.body.password;

		}).then(function() {
			// Find the user that is trying to log in
			return api.db.query(
				'SELECT id, password, activation_status FROM l2_users WHERE username = $1 LIMIT 1',
				[reqUsername]
			).then(function(userRes) {
				if(userRes.rows.length < 1) throw new ResponseError('Invalid username or password', 401);
				if(userRes.rows[0].password == null) throw new ResponseError('Invalid username or password', 401);
				return userRes.rows[0];
			});

		}).then(function(user) {
			// Verify the password
			var savedPassword = password.fromHash(user.password);
			return savedPassword.matches(reqPassword).then(function(validPass) {
				if(validPass !== true) throw new ResponseError('Invalid username or password', 401);

				// Check if the user is inactive
				if(!!user.activation_status) throw new ResponseError('This user is not activated (current status: ' + user.activation_status + ')', 401);

				return user.id;
			});

		}).then(function(userId) {
			// Setup the session
			return api.db.query(
				'SELECT sid FROM l2_sessions WHERE user_id = $1 AND LOCALTIMESTAMP < expires LIMIT 1',
				[userId]
				
			).then(function(existingRes) {
				if(existingRes.rows.length > 0) {
					// Use existing session
					var existing = existingRes.rows[0];
					return api.db.query(
						'UPDATE l2_sessions SET expires = LOCALTIMESTAMP + interval \'' + SESSION_TIMEOUT + '\' WHERE sid = $1',
						[existing.sid]
					).then(function() {
						return existing.sid;
					});

				} else {
					// Create new session
					return q.nfcall(crypto.randomBytes, 36).then(function(sidBytes) {
						var sid = sidBytes.toString('base64');
						return api.db.query(
							'INSERT INTO l2_sessions(sid, user_id, expires) VALUES ($1, $2, LOCALTIMESTAMP + interval \'' + SESSION_TIMEOUT + '\')',
							[sid, userId]
						).then(function() {
							return sid;
						});
					});

				}
				
			}).then(function(validSid) {
				// Respond
				res.cookie('l2sid', validSid, {
					httpOnly: true
				});
				res.cookie('l2uid', userId);
				res.status(200).json({sid: validSid});

			})

		}).fail(res.l2.catchError);
	});

	// Logout
	api.router.post('/session/logout', function(req, res) {
		res.l2.validateUser().then(function() {
			// Close the session
			var sid = req.get('Authorization') || req.cookies['l2sid'];
			return api.db.query(
				'UPDATE l2_sessions SET expires = LOCALTIMESTAMP WHERE sid = $1 AND LOCALTIMESTAMP < expires',
				[sid]
			);
			
		}, function(err) {
			if(err instanceof ResponseError) {
				if(err.httpStatus === 401)  return;
			}
			throw err;
			
		}).then(function() {
			// Respond
			res.clearCookie('l2sid');
			res.clearCookie('l2uid');
			res.status(200).json({});

		}).fail(res.l2.catchError);
	});

};

module.exports.SESSION_TIMEOUT = SESSION_TIMEOUT;
