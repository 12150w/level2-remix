var ResponseError = require('../errors').ResponseError,
	password = require('../password'),
	q = require('q'),
	crypto = require('crypto');

module.exports = function(api) {

	// Send invitation
	api.router.post('/users/invite', function(req, res) {
		var generatedCode, expirationDate, newUserId;

		res.l2.validateAdmin().then(function() {
			// validate request body
			if(req.body == null) throw new ResponseError('No data in request', 400);
			if(req.body.username == null) throw new ResponseError('Username is required', 400);
			if(req.body.email == null) throw new ResponseError('Email is require', 400);

		}).then(function() {
			// Check for conflicting users
			return api.db.query('SELECT id, username, email FROM l2_users WHERE LOWER(username) = LOWER($1) OR LOWER(email) = LOWER($2) LIMIT 1', [
				req.body.username, req.body.email
			]).then(function(conflictRes) {
				if(conflictRes.rows.length > 0) {
					var conflictUser = conflictRes.rows[0];
					if(conflictUser.username.toLowerCase() === req.body.username.toLowerCase())
						throw new ResponseError('That username is already registered', 400);
					else
						throw new ResponseError('That email is already registered', 400);
				}
			});

		}).then(function() {
			// Add the user
			return api.db.query('INSERT INTO l2_users (username, email, activation_status) VALUES ($1, $2, $3) RETURNING id;', [
				req.body.username, req.body.email, 'invitation sent'
			]);

		}).then(function(newUserRes) {
			// Add the invitation
			return q.ninvoke(crypto, 'randomBytes', 25).then(function(codeBuffer) {
				expirationDate = new Date();
				expirationDate.setDate(expirationDate.getDate() + 7);

				generatedCode = codeBuffer.toString('hex').toUpperCase();
				newUserId = newUserRes.rows[0].id;

				return api.db.query('INSERT INTO l2_invites (user_id, activation_code, expiration) VALUES ($1, $2, $3);', [
					newUserRes.rows[0].id, generatedCode, expirationDate
				]);
			});

		}).then(function() {
			// Check for an external site url
			return api.getExternalUrl(req);
			
		}).then(function(externalUrl) {
			// Send the activation email
			var inviteUrl = externalUrl + '/admin/#/invite/accept/' + generatedCode;

			return api.mail.send({
				to: req.body.email,
				subject: 'Invitation for ' + req.body.username,
				text: 'To accept this invitation and set your password go to  ' + inviteUrl,
				html: 'To accept this invitation and set your password <a href="' + inviteUrl + '">click here</a>'
			}).fail(function(err) {
				// Remove user and invitation
				return api.db.query('DELETE FROM l2_users WHERE id = $1;', [newUserId]).then(function() {
					throw err;
				});
			});

		}).then(function() {
			// Send back invitation details
			res.status(200).json({
				activationCode: generatedCode,
				expiration: expirationDate
			});

		}).fail(res.l2.catchError);
	});

	// Accept invitation
	var loadInviteQuery = `
		SELECT i.user_id AS user_id, i.expiration, i.claimed, u.username
		FROM l2_invites i
			INNER JOIN l2_users u ON i.user_id = u.id
		WHERE activation_code = $1
		LIMIT 1`;
	api.router.post('/users/invite/accept/:activation_code', function(req, res) {
		var invite,
			defaultStatus = null,
			currentTime = new Date(),
			requestCode = req.params.activation_code;

		api.db.query(loadInviteQuery, [requestCode]).then(function(findRes) {
			// Verify the invitation matches and is valid
			if(findRes.rows.length < 1) throw new ResponseError('Invitation not found', 404);
			invite = findRes.rows[0];

			if(invite.expiration <= currentTime) throw new ResponseError('Invitation has expired', 400);
			if(invite.claimed === true) throw new ResponseError('Invitation has expired', 400);

			// Verify there is a password in the request
			if(!req.body || !req.body.password) throw new ResponseError('No password in request', 400);

		}).then(function() {
			// Load the default activation status
			return api.settings.get('users.defaultStatus').then(function(foundStatus) {
				if(!!foundStatus) defaultStatus = foundStatus;
			});

		}).then(function() {
			// Encrypts the password
			return password.fromRaw(req.body.password).then(function(pass) {
				return pass.hashed;
			});

		}).then(function(hashedPassword) {
			// Update the user and invitation
			return api.db.query(`
				UPDATE l2_users
					SET password = $1, activation_status = $2
					WHERE id = $3`
				, [ hashedPassword, defaultStatus, invite.user_id ]
			);

		}).then(function() {
			// Update the invitation
			return api.db.query('UPDATE l2_invites SET claimed = TRUE WHERE activation_code = $1', [
				requestCode
			]);

		}).then(function() {
			// Respond
			res.status(200).json({
				username: invite.username,
				activation_status: defaultStatus
			});

		}).fail(res.l2.catchError);
	});
	api.router.get('/users/invite/accept/:activation_code', function(req, res) {
		var responseData = {},
			currentTime = new Date();

		return api.db.query(loadInviteQuery, [req.params.activation_code]).then(function(inviteRes) {
			// Set up the invitation data
			if(inviteRes.rows.length < 1) throw new ResponseError('Invitation not found', 404);
			var invite = inviteRes.rows[0];

			if(invite.claimed === true) throw new ResponseError('Invitation has expired', 400);
			if(invite.expiration <= currentTime) throw new ResponseError('Invitation has expired', 400);

			responseData.username = invite.username;

		}).then(function() {
			// Respond
			res.status(200).json(responseData);

		}).fail(res.l2.catchError);
	});

	// Get user info
	var userLoadQuery = 'SELECT id, username, email, activation_status, firstname, lastname FROM l2_users ORDER BY username';
	api.router.get('/users/manage', function(req, res) {
		var resData = {};

		return res.l2.validateAdmin().then(function() {
			// Load the users
			return api.db.query(userLoadQuery).then(function(res) {
				resData.users = res.rows;
			});

		}).then(function() {
			// Send data
			res.status(200).json(resData);

		}).fail(res.l2.catchError);
	});

	// Get user details
	api.router.get('/users/details/:username', function(req, res) {
		var data;
		
		res.l2.validateAdmin().then(function() {
			// Query the user
			return api.db.query('SELECT id, username, email, activation_status, firstname, lastname FROM l2_users WHERE username = $1',
				[req.params.username]
			).then(function(userRes) {
				if(userRes.rows.length < 1) throw new ResponseError('No user found with that username', 404);
				
				data = userRes.rows[0];
			});

		}).then(function() {
			// Query the memberships
			return api.db.query(
				'SELECT g.key, m.user_id IS NOT NULL AS is_member '
				+ 'FROM l2_groups AS g '
				+ 'LEFT OUTER JOIN l2_memberships AS m ON m.group_key = g.key AND m.user_id = $1',
				[data.id]
			).then(function(membershipRes) {
				data.memberships = membershipRes.rows;
			});
			
		}).then(function() {
			// Respond
			res.status(200).json(data);

		}).fail(res.l2.catchError);
	});

	// Admin update user
	api.router.post('/users/update/:user_id', function(req, res) {
		res.l2.validateAdmin().then(function() {
			// Prepare the body
			if(!req.body.activation_status) req.body.activation_status = null;

		}).then(function() {
			// Check for an updated password
			if(!!req.body.new_password) {
				return password.fromRaw(req.body.new_password);
			} else {
				return null;
			}

		}).then(function(updatePw) {
			// Run the update
			var updateSql = 'UPDATE l2_users SET activation_status = $1 ',
				updateParams = [req.body.activation_status, req.params.user_id],
				paramCount = 2;

			if(updatePw != null) {
				updateSql += ', password = $' + (++paramCount) + ' ';
				updateParams.push(updatePw.hashed);
			}
			
			// check for updated name
			if(req.body.firstname !== undefined) {
				updateSql += ', firstname = $' + (++paramCount) + ' ';
				updateParams.push(req.body.firstname === '' ? null : req.body.firstname);
			}
			if(req.body.lastname !== undefined) {
				updateSql += ', lastname = $' + (++paramCount) + ' ';
				updateParams.push(req.body.lastname === '' ? null : req.body.lastname);
			}

			// Finalize query
			updateSql += 'WHERE id = $2 ';

			return api.db.query(updateSql, updateParams).then(function(updateRes) {
				if(updateRes.rowCount < 1) throw new ResponseError('No user with id ' + req.params.user_id, 404);
			});

		}).then(function() {
			// Update memberships
			if(req.body.memberships == null) return;
			var bodyKeys = req.body.memberships,
				deleteKeys = [],
				addKeys = [];
			
			return api.db.query(
				'SELECT group_key FROM l2_memberships WHERE user_id = $1',
				[req.params.user_id]
				
			).then(function(membershipRes) {
				var currentKeys = membershipRes.rows.map(function(membership) { return membership.group_key; });
				
				// Load keys to create
				bodyKeys.forEach(function(groupKey) {
					if(currentKeys.indexOf(groupKey) > -1) return;
					addKeys.push(groupKey);
				});
				
				// Load keys to delete
				currentKeys.forEach(function(groupKey) {
					if(bodyKeys.indexOf(groupKey) > -1) return;
					deleteKeys.push(groupKey);
				});
				
			}).then(function() {
				// Delete memberships if needed
				if(deleteKeys.length < 1) return;
				var deleteParams = [req.params.user_id],
					deleteSql = 'DELETE FROM l2_memberships WHERE user_id = $1 AND ';
				
				var delSqlList = [],
					delCount = 1;
				deleteKeys.forEach(function(groupKey) {
					delSqlList.push('group_key = $' + (++delCount));
					deleteParams.push(groupKey);
				});
				deleteSql += '(' + delSqlList.join(' OR ') + ') ';
				
				return api.db.query(deleteSql, deleteParams);
				
			}).then(function() {
				// Add memberships if needed
				if(addKeys.length < 1) return;
				var addParams = [req.params.user_id],
					addSql = 'INSERT INTO l2_memberships(group_key, user_id) VALUES ';
				
				var addPieces = [],
					addCount = 1;
				addKeys.forEach(function(groupKey) {
					addPieces.push('($' + (++addCount) + ', $1)');
					addParams.push(groupKey);
				});
				addSql += addPieces.join(', ');
				
				return api.db.query(addSql, addParams).fail(function(err) {
					if(err.constraint === 'l2_memberships_group_key_fkey' && err.code === '23503') {
						throw new ResponseError('Invalid group key in memberships', 400);
					} else {
						throw err;
					}
				});
				
			});
			
		}).then(function() {
			// Respond
			res.status(200).json({});

		}).fail(res.l2.catchError);
	});

	// Group loading
	api.router.get('/users/all-groups', function(req, res) {
		res.l2.validateAdmin().then(function() {
			// Query the groups
			return api.db.query('SELECT key FROM l2_groups ORDER BY key').then(function(groupRes) {
				return groupRes.rows;
			});
			
		}).then(function(allGroups) {
			// Respond
			res.status(200).json({
				groups: allGroups
			});
			
		}).fail(res.l2.catchError);
	});
	
	// Group creating and delete
	api.router.post('/users/create-group', function(req, res) {
		res.l2.validateAdmin().then(function() {
			// Verify the body
			if(!req.body || !req.body.key) {
				throw new ResponseError('key is required to create a group', 400);
			}
			
		}).then(function() {
			// Check for an existing group
			return api.db.query('SELECT key FROM l2_groups WHERE key = $1 LIMIT 1', [req.body.key]).then(function(findRes) {
				if(findRes.rows.length > 0) {
					throw new ResponseError('A group with the key "' + req.body.key + '" already exists', 400);
				}
			});
			
		}).then(function() {
			// Create the group
			return api.db.query('INSERT INTO l2_groups(key) VALUES ($1)', [req.body.key]);
			
		}).then(function() {
			// Respond
			res.status(200).json({});
			
		}).fail(res.l2.catchError);
	});
	api.router.post('/users/delete-group', function(req, res) {
		res.l2.validateAdmin().then(function() {
			// Validate the body
			if(!req.body && !req.body.key) {
				throw new ResponseError('No group key in request', 400);
			}
			
		}).then(function() {
			// Delete the group
			return api.db.query('DELETE FROM l2_groups WHERE key = $1', [req.body.key]);
			
		}).then(function() {
			// Respond
			res.status(200).json({});
			
		}).fail(res.l2.catchError);
	});
	
};
