/* 
 * Admin Services
 *    Admin general services
 * 
 */
var ResponseError = require('../errors').ResponseError,
	Password = require('../password'),
	q = require('q'),
	crypto = require('crypto');

module.exports = function(api) {
	
	// Admin Login
	api.router.post('/admin-api/login', function(req, res) {
		var now = new Date();
		
		// Validate body
		if(!req.body) return res.l2.sendError('No login details in request', 400);
		if(!req.body.username) return res.l2.sendError('Please enter a username', 400);
		if(!req.body.password) return res.l2.sendError('Please enter a password', 400);
		
		var setupAdmin, respondSID;
		return api.settings.get('setup.admin').then(function(admin) {
			setupAdmin = admin;
			
			// Identity
			if(!setupAdmin) throw new ResponseError('Admin user is not set up yet', 400);
			if(setupAdmin.username !== req.body.username) throw new ResponseError('Invalid username or password', 401);
			
			var adminPassword = Password.fromHash(setupAdmin.password);
			return adminPassword.matches(req.body.password);
		}).then(function(pwMatches) {
			if(pwMatches !== true) throw new ResponseError('Invalid username or password', 401);
			
			// Find existing session
			return api.settings.get('session');
		}).then(function(savedSession) {
			
			// Re-Use SID if needed
			if(
				!!savedSession
				&& !!savedSession.sid
				&& typeof savedSession.validUntil === 'number'
			) {
				if(new Date(savedSession.validUntil) > now)
					return savedSession.sid;
			}
			
			// or Generate new SID
			return q.ninvoke(crypto, 'randomBytes', 45).then(function(rawBytes) {
				return rawBytes.toString('base64');
			});
			
		}).then(function(unsafeSID) {
			// Remove any unwanted characters
			return unsafeSID.replace(/\//g, 'A').replace(/\+/g, 'B').replace(/=/g, 'C');
			
		}).then(function(validSID) {
			respondSID = validSID;
			
			// Save the valid session
			now.setHours(now.getHours() + 3);
			return api.settings.set('session', {
				sid: respondSID,
				validUntil: now.getTime()
			});
		}).then(function() {
			
			res.cookie('l2uid_adm', respondSID, {
				expires: now
			});
			res.status(200).json({
				sid: respondSID
			});
		}).fail(res.l2.catchError);
	});
	
};