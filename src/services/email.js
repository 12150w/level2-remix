/*
 * Email Service
 *    Manages sending emails and managing email settings
 *
 */

module.exports = function(api) {

    // Load email settings
    api.router.get('/email/settings', function(req, res) {
        var emailSettings;
        
        res.l2.validateAdmin().then(function() {
            // Load the email settings
            return api.settings.get('email');

        }).then(function(readSettings) {
            // Load externalUrl
            emailSettings = readSettings || {};
            return api.settings.get('externalUrl');
            
        }).then(function(externalUrl) {
            // Respond
            res.status(200).json({
                transport: emailSettings.transport || null,
                options: emailSettings.options || null,
				defaultFrom: emailSettings.defaultFrom || null,
                externalUrl: externalUrl || null
            });

        }).fail(res.l2.catchError);
    });

    // Save email settings
    api.router.post('/email/settings', function(req, res) {
        res.l2.validateAdmin().then(function() {
            // Save the settings
            return api.settings.set('email', {
                transport: req.body.transport || null,
                options: req.body.options || null,
				defaultFrom: req.body.defaultFrom || null
            }).then(function() {
                return api.settings.set('externalUrl', req.body.externalUrl || null);
            });

        }).then(function() {
            // Respond
            res.status(200).json({});

        }).fail(res.l2.catchError);
    });

};
