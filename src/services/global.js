/*
 * Global Handlers
 *
 * Run for all requests sent through the API
 */
var attachReqContext = require('./req-context'),
	bodyParser = require('body-parser'),
	express = require('express'),
	path = require('path');

module.exports = function(api) {

	// Handle static files
	api.router.use('/vendor', express.static(path.join(__dirname, '..', '..', 'admin', 'bower_components')));
	api.router.use('/admin', express.static(path.join(__dirname, '..', '..', 'admin', 'dist')));
	api.router.get('/favicon.ico', function(req, res) {
		res.sendFile(path.join(__dirname, '..', '..', 'admin', 'dist', 'assets', 'favicon.ico'));
	});
	api.router.use('/admin+', function(req, res) {
		res.sendFile(path.join(__dirname, '..', '..', 'admin', 'dist', 'index.html'));
	});

	// Add l2 request context
	attachReqContext(api.router, api);

	// Check for setup route
	api.router.use('/setup', function(req, res, next) {
		res.l2.isSetupRoute = true;
		next();
	});

	// Parse the body
	api.router.use(bodyParser.json());

	// Check for setup complete
	api.router.use(function(req, res, next) {
		api.settings.get('setup.complete').then(function(val) {
			if(val !== true && res.l2.isSetupRoute !== true) {
				res.l2.sendError('The initial server setup is incomplete', 400, 'NOT_SETUP');
			} else {
				next();
			}
		}).fail(res.l2.catchError);
	});

};
