/*
 * Setup Service
 *
 */
var errors = require('../errors'),
	pg = require('pg'),
	q = require('q'),

	Password = require('../password');

module.exports = function(api) {

	// Database Setup
	api.router.post('/setup/database', function(req, res) {
		var pgSettings;

		api.settings.get('postgres').then(function(dbConfig) {
			if(dbConfig !== undefined) throw new errors.ResponseError('The database is already setup', 400);

			// Verify format
			if(!req.body || !req.body.database) throw new errors.ResponseError('Invalid request body', 400);

			// Merge settings
			pgSettings = {database: req.body.database};
			if(!!req.body.host) pgSettings.host = req.body.host;
			if(!!req.body.port) pgSettings.port = req.body.port;
			if(!!req.body.user) pgSettings.user = req.body.user;
			if(!!req.body.password) pgSettings.password = req.body.password;

			// Test the connection
			var testClient = new pg.Client(pgSettings);
			return q.ninvoke(testClient, 'connect').then(function() {
				testClient.end();
			}).fail(function(err) {
				var stringErr = String(err);
				stringErr = stringErr.replace('error: ', '').replace('Error: ', '');
				stringErr = 'Invalid database configuration: ' + stringErr;

				throw new errors.ResponseError(stringErr, 400);
			});

		}).then(function(client) {
			// Persist the config
			return api.settings.set('postgres', pgSettings);

		}).then(function() {
			// Setup internal data model
			return api.db.upgrade();

		}).then(function() {
			res.status(200).json({});

		}).fail(res.l2.catchError);
	});

	// Admin User Setup
	api.router.post('/setup/admin', function(req, res) {

		// Verify body
		if(!req.body) return res.l2.sendError('No request body', 400);
		if(!req.body.username) return res.l2.sendError('Username is required', 400);
		if(!req.body.password) return res.l2.sendError('Password is required', 400);
		if(!req.body.email) return res.l2.sendError('Email is required', 400);

		api.settings.get('setup.admin').then(function(existingAdmin) {
			// Verify the admin is not already setup
			if(existingAdmin !== undefined) throw new errors.ResponseError('The admin account has already been created', 400);

		}).then(function() {
			// Encrypt the password
			return Password.fromRaw(req.body.password);

		}).then(function(adminPassword) {
			// Store the admin user
			return api.settings.set('setup.admin', {
				username: req.body.username,
				password: adminPassword.hashed,
				email: req.body.email
			});

		}).then(function() {
			// Flag setup as complete
			return api.settings.set('setup.complete', true);

		}).then(function() {
			// Respond
			res.status(200).json({});

		}).fail(res.l2.catchError);
	});

	// Setup Status
	api.router.get('/setup/status', function(req, res) {
		var database = undefined;

		api.settings.get('postgres.database').then(function(dbName) {
			database = dbName;
			return api.settings.get('setup.admin.username');

		}).then(function(admin) {
			if(database === undefined) return res.status(200).json({status: null});
			if(admin === undefined) return res.status(200).json({status: 'db'});
			return res.status(200).json({status: 'complete'});

		}).fail(res.l2.catchError);
	});

	// Panel Overview
	api.router.get('/setup/overview', function(req, res) {
		var panelData = {
			users: {},
			db: {}
		};

		res.l2.validateAdmin().then(function() {
			// Load the admin
			return api.settings.get('setup.admin').then(function(admin) {
				panelData.admin = {
					username: admin.username,
					email: admin.email
				};
			});

		}).then(function() {
			// Load total users
			return api.db.query('SELECT COUNT(*) AS user_count FROM l2_users').then(function(res) {
				panelData.users.count = parseInt(res.rows[0].user_count, 10);
			});

		}).then(function() {
			// Load the database overview
			return api.settings.get('externalMigrationIndex').then(function(externalIndex) {
				panelData.db.externalIndex = externalIndex || null;
				return api.settings.get('setup.internalMigrationIndex');

			}).then(function(internalIndex) {
				panelData.db.internalIndex = internalIndex || null;

			});

		}).then(function() {
			// Send the data
			res.status(200).json(panelData);

		}).fail(res.l2.catchError);
	});

};
