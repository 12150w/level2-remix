/*
 * Account Service
 *   Provides account related function
 *
 */
var ResponseError = require('../errors').ResponseError,
	Password = require('../password'),
	crypto = require('crypto'),
	q = require('q');

module.exports = function(api) {
	
	// PRR Request
	var existingPRRQuery = '\
		SELECT code \
		FROM l2_prr \
		WHERE \
			expiration >= LOCALTIMESTAMP \
			AND status IS NULL \
			AND user_id = $1 \
		LIMIT 1';
	api.router.post('/account/prr/request', function(req, res) {
		var prrUser, prrCode;
		
		q().then(function() {
			// Validate the username
			if(!req.body.username) {
				throw new ResponseError('No username specified', 400);
			}
			
			return api.db.query('SELECT id, email, username FROM l2_users WHERE username = $1 LIMIT 1', [
				req.body.username
			]).then(function(userRes) {
				if(userRes.rows.length < 1)
					throw new ResponseError('Unable to reset password, please try again later', 400);
				
				prrUser = userRes.rows[0];
			});
			
		}).then(function() {
			// Check for an active PRR
			return api.db.query(existingPRRQuery, [prrUser.id]).then(function(prrRes) {
				if(prrRes.rows.length > 0) {
					throw new ResponseError('Unable to reset password, please try again later', 400);
				}
			});
			
		}).then(function() {
			// Generate the PRR code
			return q.nfcall(crypto.randomBytes, 75);
			
		}).then(function(prrCodeBuf) {
			// Create the new PRR
			prrCode = prrCodeBuf.toString('hex').toUpperCase();
			
			return api.db.query('SELECT LOCALTIMESTAMP AS dbtime').then(function(timeRes) {
				var prrExpiration = timeRes.rows[0].dbtime;
				prrExpiration.setHours(prrExpiration.getHours() + 2);
				
				return prrExpiration;
				
			}).then(function(prrExpiration) {
				return api.db.query('INSERT INTO l2_prr(user_id, code, expiration) VALUES ($1, $2, $3)', [
					prrUser.id, prrCode, prrExpiration
				]);
				
			});
			
		}).then(function() {
			// Email the PRR code
			return api.getExternalUrl(req).then(function(externalUrl) {
				var resetUrl = externalUrl + '/admin/#/reset-password/' + prrCode,
					warningText = 'IF YOU DID NOT REQUEST TO CHANGE YOUR PASSWORD PLEASE CONTACT YOUR ADMINISTRATOR IMMEDIATELY';
				
				return api.mail.send({
					to: prrUser.email,
					subject: 'Password Reset for ' + prrUser.username,
					text: 'To reset your password go to ' + resetUrl + '\n\n' + warningText,
					html: '<p>To reset your password <a href="' + resetUrl + '">click here</a></p><p><b>' + warningText + '</b></p>'
				});
			});
			
		}).then(function() {
			// Respond
			res.status(200).json({});
			
		}).fail(res.l2.catchError)
	});
	
	// PRR Execute
	var prrExecuteFind = '\
		SELECT user_id\
		FROM l2_prr\
		WHERE\
			expiration >= LOCALTIMESTAMP\
			AND status IS NULL\
			AND code = $1\
		LIMIT 1';
	var userUpdateQuery = '\
		UPDATE l2_users\
		SET\
			password = $1\
		WHERE\
			id = $2';
	api.router.post('/account/prr/execute', function(req, res) {
		var userId;
		
		q().then(function() {
			// Verify request
			if(!req.body.code) {
				throw new ResponseError('No PRR code specified', 400);
			}
			if(!req.body.password) {
				throw new ResponseError('No password was specified', 400);
			}
			
		}).then(function() {
			// Verify the PRR code
			return api.db.query(prrExecuteFind, [req.body.code]).then(function(verifyReq) {
				if(verifyReq.rows.length < 1) {
					throw new ResponseError('Invalid PRR code', 400);
				}
				userId = verifyReq.rows[0].user_id;
			});
			
		}).then(function() {
			// Hash the new password
			return Password.fromRaw(req.body.password);
			
		}).then(function(newPw) {
			// Update the user's record
			return api.db.query(userUpdateQuery, [newPw.hashed, userId]);
			
		}).then(function() {
			// De-activate the PRR
			return api.db.query("UPDATE l2_prr SET status = 'S' WHERE code = $1", [
				req.body.code
			]);
			
		}).then(function() {
			// Email the user to notify of password update
			api.db.query('SELECT email, username FROM l2_users WHERE id = $1 LIMIT 1', [
				userId
			]).then(function(emailRes) {
				var userEmail = emailRes.rows[0].email,
					warning = 'if YOU did not change the password please contact your administrator.';
				
				return api.mail.send({
					to: userEmail,
					subject: 'Password Change Notice',
					text: 'The password for ' + username + ' has been updated.\n\n' + warning,
					html: '<p>The password for ' + username + ' has been updated.</p><p><b>' + warning + '</b></p>'
				});
			});
			
		}).then(function() {
			// Respond
			res.status(200).json({});
			
		}).fail(res.l2.catchError);
	});
	
};
