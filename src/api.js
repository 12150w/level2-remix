var express = require('express'),
	glob = require('glob'),
	q = require('q'),
	fs = require('fs'),
	os = require('os'),
	base = require('./base'),
	Settings = require('./settings'),
	Database = require('./db'),
	Mailer = require('./mailer'),
	ResponseError = require('./errors').ResponseError,
	
	attachRequestContext = require('./services/req-context'),

	attachRootHandlers = require('./services/global'),
	attachSetupHandlers = require('./services/setup'),
	attachAdminHandlers = require('./services/admin'),
	attachUsersHandlers = require('./services/users'),
	attachEmailHandlers = require('./services/email'),
	attachSessionHandlers = require('./services/session'),
	attachDataHandlers = require('./services/data'),
	attachAccountServiceHandlers = require('./services/account');

module.exports = base.Class.extend({
	init: function(options) {
		this.router = express();
		this._finalLoaded = false;
		this._options = options || {};
		this._netServer = null;
		this._exposedCollections = {};

		// Settings and Database
		this._useSettingsPath = this._options.settingsPath || 'level2-remix-settings.json';
		this.settings = new Settings(this._useSettingsPath);
		this.db = new Database(this, this._options.dbOptions);
		this._logPath = this._options.logPath || 'level2-log.txt';
		this._autoUpdate = true;

		// Mailer
		this.mail = new Mailer(this);

		// Attach built in services
		this._attachBuiltInServices();

		// Finalize if sub app
		this.router.on('mount', this._finalize.bind(this));
	},

	_attachBuiltInServices: function() {

		// Global Handlers
		attachRootHandlers(this);

		// Root service
		this.router.get('/', function(req, res) {
			res.status(200).json({});
		});

		// All other services
		attachSetupHandlers(this);
		attachAdminHandlers(this);
		attachUsersHandlers(this);
		attachEmailHandlers(this);
		attachSessionHandlers(this);
		attachDataHandlers(this);
		attachAccountServiceHandlers(this);
	},

	listen: function() {
		this._finalize();
		var listenDefer = q.defer();

		// Merge together arguments
		var listenArgs = [];
		for(var i=0; i<arguments.length; i++) {
			listenArgs.push(arguments[i]);
		}
		listenArgs.push(function(err) {
			if(err == null) listenDefer.resolve();
			else listenDefer.reject(err);
		});

		this._netServer = this.router.listen.apply(this.router, listenArgs);
		return listenDefer.promise;
	},

	logMessage: function(message, source) {
		source = !source ? '' : '[' + source.toUpperCase() + '] ';

		return this._logOutput(source + message);
	},

	logError: function(message, source) {
		source = !source ? '' : '[' + source.toUpperCase() + '] ';

		return this._logOutput('ERROR ' + source + message);
	},

	services: function(serviceFolder) {
		var serviceRouter = express.Router();
		
		attachRequestContext(serviceRouter, this);
		serviceRouter.use(function(req, res, next) {
			res.l2._externalService = true;
			next();
		});
		
		var serviceFiles = glob.sync('**/*.js', {cwd: serviceFolder});
		for(var i=0; i<serviceFiles.length; i++) {
			require(serviceFolder + '/' + serviceFiles[i])(serviceRouter, this);
		}
		
		serviceRouter.use(function(req, res) {
			res.l2.sendError('No service found', 404);
		});
		
		return serviceRouter;
	},
	
	getExternalUrl: function(req) {
		return this.settings.get('externalUrl').then(function(savedUrl) {
			if(!!savedUrl) {
				// Use the saved URL
				if(/\/$/.exec(savedUrl) != null) {
					savedUrl = savedUrl.substring(0, savedUrl.length - 1);
				}
				return savedUrl;
				
			} else {
				// Try to figure out the URL
				return req.protocol + '://' + req.hostname + req.baseUrl;
				
			}
		});
	},
	
	exposeCollection: function(collection) {
		this._exposedCollections[collection] = true;
	},
	
	_logOutput: function(line) {
		// Writes a line to the log file
		return q.nfcall(fs.appendFile, this._logPath, line + os.EOL, {encoding: 'utf8'});
	},

	_finalize: function() {
		// Adds in the after endpoint handlers
		if(this._finalLoaded === true) return;

		// Not found handler
		this.router.use(function(req, res) {
			res.l2.sendError('No endpoint handler found for that URL or method', 404, 'INVALID_ENDPOINT');
		});

		// Uncaught Exception handler
		this.router.use(function(err, req, res, next) {
			if(!res.l2) throw err;
			res.l2.catchError(err);
		});

		// Attempt migration
		if(this._autoUpdate === true) {
			this.db.upgrade().fail(function(err) {
				if(err instanceof ResponseError) {
					if(err.message !== 'Database configuration could not be found') {
						throw err;
					}
				} else {
					throw err;
				}
			}).done();
		}

		this._finalLoaded = true;
	}
});
