var bcrypt = require('bcryptjs'),
	q = require('q'),
	base = require('./base');

var Password = base.Class.extend({
	init: function(hashed) {
		this.hashed = hashed;
	},
	
	matches: function(comparePassword) {
		return q.ninvoke(bcrypt, 'compare', comparePassword, this.hashed);
	}
});

Password.fromRaw = function(rawPassword) {
	return q.ninvoke(bcrypt, 'genSalt', 10, null).then(function(salt) {
		return q.ninvoke(bcrypt, 'hash', rawPassword, salt);
		
	}).then(function(hash) {
		return new Password(hash);
		
	});
};

Password.fromHash = function(hashedPassword) {
	return new Password(hashedPassword);
};

module.exports = Password;
