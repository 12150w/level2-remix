/*
 * Mailer
 *    Interface for sending messages
 *
 */
var base = require('./base'),
    nodemailer = require('nodemailer'),
    stubTransport = require('nodemailer-stub-transport'),
    q = require('q');

module.exports = base.Class.extend(base.EventEmitter, {
    init: function(contextApi) {
        this._api = contextApi;
    },

    send: function(mailOptions) {
        var self = this;

        mailOptions = mailOptions || {};
        var mailSetup;

        // Load the mail settings
        return self._api.settings.get('email').then(function(readSetup) {
            // Create the transporter
            mailSetup = readSetup || {};

            if(mailSetup.transport === 'smtp') {
                var smtpSetup = JSON.parse(JSON.stringify(mailSetup.options));
                delete smtpSetup.authUser;
                delete smtpSetup.authPass;

                if(!!mailSetup.options.authUser) {
                    smtpSetup.auth = smtpSetup.auth || {};
                    smtpSetup.auth.user = mailSetup.options.authUser;
                }
                if(!!mailSetup.options.authPass) {
                    smtpSetup.auth = smtpSetup.auth || {};
                    smtpSetup.auth.pass = mailSetup.options.authPass;
                }

                return nodemailer.createTransport(smtpSetup || {});
            } else if(mailSetup.transport === 'direct') {
                return nodemailer.createTransport();
            } else {
                return nodemailer.createTransport(stubTransport());
            }

        }).then(function(transport) {
            // Send the email
			if(!mailOptions.from) mailOptions.from = mailSetup.defaultFrom || null;
            return q.ninvoke(transport, 'sendMail', mailOptions);

        }).then(function(results) {
            // Check if mail event should be emitted
            self.emit('mail', results);

            return results;

        });
    }
});
