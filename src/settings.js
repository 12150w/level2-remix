/* 
 * Settings
 *    Stores persistent server settings
 * 
 */
var base = require('./base'),
	q = require('q'),
	fs = require('fs');

module.exports = base.Class.extend({
	_cache: {},
	_filepath: null,
	_loaded: false,
	
	init: function(filePath) {
		this._filepath = filePath
	},
	
	get: function(path) {
		return this._verifyLoaded().then(function() {
			return this._retrievePath(path);
		}.bind(this));
	},
	
	getCached: function(path) { return this._retrievePath(path); },
	
	set: function(path, value) {
		return this._verifyLoaded().then(function() {
			// Merge into the cache
			var setProblem = this._injectPath(path, value);
			if(setProblem !== null) throw new Error(setProblem);
			
			return q.nfcall(fs.writeFile, this._filepath, this._getSerializedCache(), {encoding: 'utf8'});
		}.bind(this));
	},
	
	reload: function() {
		var loadDefer = q.defer();
		
		fs.readFile(this._filepath, function(err, raw) {
			if(!!err && err.code !== 'ENOENT') return loadDefer.reject(err);
			this._deserializeToCache(raw);
			this._loaded = true;
			
			loadDefer.resolve();
		}.bind(this));
		
		return loadDefer.promise;
	},
	
	getFilePath: function() {
		return this._filepath;
	},
	
	_injectPath: function(path, value) {
		if(path === null || path === undefined || path === NaN) return 'Path can not be null';
		if(typeof path !== 'string') return 'Path must be a String';
		
		var currentNode = this._cache,
			nodePaths = path.split('.'),
			_i, path;
		
		for(_i=0; _i<nodePaths.length; _i++) {
			path = nodePaths[_i];
			
			if(_i === nodePaths.length - 1) {
				currentNode[path] = value;
				break;
			}
			
			if(typeof currentNode[path] !== 'object') {
				currentNode[path] = {}
			}
			currentNode = currentNode[path];
		}
		
		return null;
	},
	
	_retrievePath: function(path) {
		if(path === null || path === undefined || path === NaN) throw new Error('Path can not be null');
		if(typeof path !== 'string') throw new Error('Path must be a String');
		
		var currentNode = this._cache,
			nodePaths = path.split('.'),
			found = true,
			_i, path, value;
		
		for(_i=0; _i<nodePaths.length; _i++) {
			path = nodePaths[_i];
			
			if(_i === nodePaths.length - 1) {
				value = currentNode[path];
				break;
			}
			
			if(typeof currentNode[path] !== 'object') {
				found = false;
				break;
			}
			currentNode = currentNode[path];
		}
		
		//if(found === false) throw new Error('Value not found for path "' + path + '"');
		if(found === false) return undefined;
		return value;
	},
	
	_getSerializedCache: function() {
		return JSON.stringify(this._cache, null, '    ');
	},
	
	_deserializeToCache: function(raw) {
		try {
			this._cache = JSON.parse(raw);
		} catch(e) {
			if(e instanceof SyntaxError) this._cache = {};
			else throw e;
		}
	},
	
	_verifyLoaded: function() {
		if(this._loaded !== true) return this.reload();
		else return q.fcall(function() {});
	}
});