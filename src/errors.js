/* 
 * Errors used in level-remix
 * 
 */
var util = require('util');

module.exports.ResponseError = function(message, httpStatus, errorCode, extraAttributes) {
	Error.call(this);
	Error.captureStackTrace(this, this.constructor);
	
	this.message = message;
	this.httpStatus = httpStatus;
	this.errorCode = errorCode;
	this.extraAttributes = extraAttributes;
};
util.inherits(module.exports.ResponseError, Error);
module.exports.ResponseError.prototype.name = 'ResponseError';

module.exports.HTTPError = function(res) {
	Error.call(this);
	
	this.msg = res.msg;
	this.body = res.body;
	if(typeof(this.body) === 'string') this.body = JSON.parse(this.body);
	
	this.message = 'HTTP ' + this.msg.statusCode + ' [' + this.body.code + '] ' + this.body.msg;
	this.stack = (this.body || {}).stack;
};
util.inherits(module.exports.HTTPError, Error);
module.exports.HTTPError.prototype.name = 'HTTPError';
