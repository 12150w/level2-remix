/* 
 * Simple Utility Functions
 * 
 */

var NUMBER_DASH_FORMAT = /^(\d+?)-.*/;
module.exports.numberDashSort = function(me, other) {
	// Sorts strings starting with "1-..."
	var meMatch = NUMBER_DASH_FORMAT.exec(me),
		otherMatch = NUMBER_DASH_FORMAT.exec(other);
	
	// Check for non matching format
	if(meMatch === null) throw new Error('Invalid number dash item "' + String(me) + '"');
	if(otherMatch === null) throw new Error('Invalid number dash item "' + String(other) + '"');
	var meVal = parseInt(meMatch[1], 10),
		otherVal = parseInt(otherMatch[1], 10);
	
	return meVal - otherVal;
};