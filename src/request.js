/* 
 * A Promise wrapper for the request function
 * 
 */
var q = require('q'),
	request = require('request'),
	errors = require('./errors');

module.exports = function(options) {
	return q.nfcall(request, options).then(function(resultList) {
		var res = {
			msg: resultList[0],
			body: resultList[1]
		};
		
		// Check for HTTP Error
		if(res.msg.statusCode >= 400) {
			throw new errors.HTTPError(res);
		} else {
			return res;
		}
	});
};
