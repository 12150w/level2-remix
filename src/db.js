/*
 * Database Promise Layer
 *
 * Exposes the Postgres database with a promise API
 *
 */
var base = require('./base'),
	errors = require('./errors'),
	util = require('./util'),
	DataStore = require('./data-store'),
	path = require('path'),
	fs = require('fs'),
	q = require('q'),
	pg = require('pg');

module.exports = base.Class.extend({
	init: function(api, options) {
		this._api = api;
		this._client = null;
		this._externalMigrationFolder = null;

		this._options = options || {};
		this._options.singleClient = this._options.singleClient === true ? true : false;

		this.store = new DataStore(this);
	},

	query: function(sql, parameters) {
		parameters = parameters || [];
		var conn;

		return this._getConn().then(function(loadedConnection) {
			// Run the query
			conn = loadedConnection;
			return q.ninvoke(conn.client, 'query', sql, parameters);

		}).then(function(queryRes) {
			// Release the client
			conn.release();
			return queryRes;

		});
	},

	upgrade: function() {
		var self = this;

		return self._loadMigrations(path.join(__dirname, 'migrations'), 'setup.internalMigrationIndex').then(function() {
			if(!self._externalMigrationFolder) return;

			return self._loadMigrations(self._externalMigrationFolder, 'externalMigrationIndex');
		});
	},

	setMigrationPath: function(migrationPath) {
		this._externalMigrationFolder = migrationPath;
	},

	changeSchema: function(schemaName) {
		return this.query('SET search_path TO ' + schemaName);
	},

	_loadMigrations: function(migrationDirectory, indexKey) {
		var self = this,
			migrationIndex = 0;

		return self._api.settings.get(indexKey).then(function(readIndex) {
			// Get the current migration index
			if(readIndex !== undefined) migrationIndex = readIndex;

		}).then(function() {
			// Load the migrations list
			return q.nfcall(fs.readdir, migrationDirectory);

		}).then(function(migrationPaths) {
			migrationPaths = migrationPaths.sort(util.numberDashSort);
			migrationPaths.splice(0, migrationIndex);

			// Iterate through the migration paths
			return migrationPaths.reduce(function(prevPromise, migrationFilePath) {
				return prevPromise.then(function() {
					var migrationPath = path.join(migrationDirectory, migrationFilePath);

					// Read this migration
					return q.nfcall(fs.readFile, migrationPath, {encoding: 'utf8'}).then(function(sql) {
						return self.query(sql);
					}).then(function() {
						self._api.logMessage('Migration successful ' + migrationFilePath, 'MIGRATION');
						return self._api.settings.set(indexKey, ++migrationIndex);
					}).fail(function(err) {
						self._api.logError('Migration failed ' + migrationFilePath + ': ' + err, 'MIGRATION');
						throw err;
					});
				});
			}, q());
		});
	},

	_loadConfig: function() {
		// Returns a promise for the database config object
		return this._api.settings.get('postgres').then(function(config) {
			config = config || {};
			if(!!process.env['L2_PG_HOST']) config.host = process.env['L2_PG_HOST'];
			if(!!process.env['L2_PG_DB']) config.database = process.env['L2_PG_DB'];
			if(!!process.env['L2_PG_USER']) config.user = process.env['L2_PG_USER'];
			if(!!process.env['L2_PG_PW']) config.password = process.env['L2_PG_PW'];

			if(!config.database) {
				throw new errors.ResponseError('Database configuration is not complete', 400, 'EXCEPTION');
			} else {
				return config;
			}
		});
	},

	_getConn: function() {
		// Returns a promise that resolves to a pg Client
		var self = this;

		// Load the single Client
		if(self._options.singleClient === true) {
			if(self._client != null) return q(self._client);

			return self._loadConfig().then(function(dbConfig) {
				self._client = {
					client: new pg.Client(dbConfig),
					release: function() {}
				};
				return q.ninvoke(self._client.client, 'connect');

			}).then(function() {
				return self._client;
			});
		}

		// Load Client from a pool
		else {
			return self._loadConfig().then(function(dbConfig) {
				return q.ninvoke(pg, 'connect', dbConfig);
			}).then(function(connInfo) {
				return {
					client: connInfo[0],
					release: connInfo[1]
				};
			});
		}
	}
});
