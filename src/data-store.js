/*
 * DataStore
 *    Provides a backing for the data service
 *
 */
var Class = require('./base').Class;

module.exports = Class.extend({

	init: function(db) {
		this._db = db;
	},

	create: function(collection, data) {
		var insertSQL = 'INSERT INTO ' + this._simpleEscape(collection) + ' ';
		var sqlData = [];

		if(data != null) {
			var insertKeys = [];
			var valueKeys = [];
			var valueIndex = 0;

			for(var field in data) {
				insertKeys.push(field);
				valueKeys.push('$' + (++valueIndex));
				sqlData.push(data[field]);
			}
			insertSQL += '(' + insertKeys.join(',') + ') ';
			insertSQL += 'VALUES (' + valueKeys.join(',') + ') ';
		}

		insertSQL += 'RETURNING * ';

		return this._db.query(insertSQL + ';', sqlData).then(function(insertRes) {
			// Respond
			var data = insertRes.rows[0],
				id = data.id;
			delete data.id;

			return {
				id: id,
				data: data
			};

		});
	},

	findById: function(collection, ids) {
		return this.query(collection, {
			id: ids
		});
	},

	query: function(collection, query) {
		var querySQL = 'SELECT * FROM ' + this._simpleEscape(collection) + ' ',
			queryData = this._buildQuerySQL(query);
		querySQL += 'WHERE ' + queryData.sql + ' ';

		return this._db.query(querySQL + ';', queryData.params).then(function(res) {
			// Prepare the results
			var records = [],
				row, record;
			for(var i=0; i<res.rows.length; i++) {
				row = res.rows[i];
				record = { id: row.id };

				delete row.id;
				record.data = row;

				records.push(record);
			}

			return { records: records };
		});
	},

	update: function(collection, id, data) {
		var self = this;
		var updateSql = 'UPDATE ' + this._simpleEscape(collection) + ' SET ';

		// Add field changes
		var fieldParams = [],
			setStatements = [];
		for(var field in data) {
			fieldParams.push(data[field]);
			setStatements.push(field + ' = $' + fieldParams.length);
		}
		updateSql += setStatements.join(', ') + ' ';

		fieldParams.push(id);
		updateSql += 'WHERE id = $' + fieldParams.length + ' ';

		// Run the update
		return this._db.query(updateSql + ';', fieldParams).then(function() {
			// Re query the record
			return self.findById(collection, [id]);

		}).then(function(res) {
			// Get the first result
			return res.records[0];

		});
	},

	delete: function(collection, id) {
		var deleteSql = 'DELETE FROM ' + this._simpleEscape(collection) + ' WHERE id = $1';

		return this._db.query(deleteSql + ';', [id]).then(function() {});
	},

	_buildQuerySQL: function(query, index, combination) {
		// Returns a query data object
		//     {sql: '...', params: [...]}
		index = index || 0;
		combination = combination || ' AND ';
		var data = {params: []};

		// Iterate through the query
		var val, i, itemSql,
			itemList = [];
		for(var key in query) {
			val = query[key];
			itemSql = '';

			// Check if this is an in filter
			if(Object.prototype.toString.call(val) === '[object Array]') {
				itemSql += key + ' IN (';
				for(i=0; i<val.length; i++) {
					itemSql += '$' + (++index) + ', ';
					data.params.push(val[i]);
				}
				if(val.length > 0) itemSql = itemSql.substring(0, itemSql.length - 2);
				itemSql += ')';
			}

			// Check for equal comparison
			else {
				itemSql += key + ' = $' + (++index);
				data.params.push(val);
			}

			itemList.push(itemSql);
		}

		// Join with combination
		data.sql = '(' + itemList.join(combination) + ') ';

		return data;
	},

	_simpleEscape: function(raw) {
		// Does simple SQL escaping
		if(!raw) return raw;

		return raw.replace(/'/g, "\\'");
	}

});
