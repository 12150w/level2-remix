/*
 * Unit Testing Util
 *    Helps make unit testing easier
 *
 */
var TestUtil = {},
	assert = require('assert');

/* Verify Requires Admin
 *    Returns a function that tests that
 *    the given endpoint returns 401 when
 *    not using an admin session
 */
TestUtil.verifyRequiresAdmin = function(url, method, api) {
	return function(done) {
		api.request({
			url: url,
			method: method
		}).then(function() {
			throw new Error('Endpoint did not return unauthorized when it should have');

		}, function(err) {
			assert.equal(err.msg.statusCode, 401, 'The response should be HTTP 401');

		}).then(function() {done();}, done).done();
	};
};

/* Verify Requires Session
 *    Returns a function that tests that a valid session id is required
 *    to access the given resource
 */
TestUtil.verifyRequiresSession = function(url, method, api) {
	return function(done) {
		api.request({
			url: url,
			method: method
		}).then(function() {
			throw new Error('Endpoint did not return unauthorized when it should have');

		}, function(err) {
			assert.equal(err.msg.statusCode, 401, 'The response should be HTTP 401');
			assert.equal(err.body.code, 'NO_LOGIN', 'Expected the NO_LOGIN error code');

		}).then(function() {done();}, done).done();
	};
};

/* Get Unique Email
 *    Returns a unique email
 *    Unique from other emails created by this method
 */
var emailCounter = 0;
TestUtil.getUniqueEmail = function() {
	return 'gen.unit.test' + (++emailCounter) + '@12150w.com';
};

module.exports = TestUtil;
