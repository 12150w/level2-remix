/* Tests the setup service
 */
var TestAPI = require('../../src/test-api'),
	TestUtil = require('../../src/test-util');
	assert = require('assert');

describe('Setup Service', function() {
	var api = new TestAPI({doSetup: true});
	before('Set up the test API', function(done) {
		api.listen().then(function() {done();}, done).done();
	});
	
	describe('Panel Endpoint (/setup/overview)', function() {
		describe('GET', function() {
			it('Requires admin login', TestUtil.verifyRequiresAdmin('/setup/overview', 'GET', api));
			
			it('Loads the admin user data', function(done) {
				api.secureGet('/setup/overview').then(function(res) {
					assert.equal(res.body.admin.username, 'test');
					assert.equal(res.body.admin.email, 'unit.test@12150w.com');
					
					done();
				}, done).done();
			});
			
			it('Loads the number of users', function(done) {
				api.secureGet('/setup/overview').then(function(res) {
					assert.ok(res.body.users.count === 0);
					
					done();
				}, done).done();
			});
		});
	});
	
	describe('Admin Setup Endpoint (/setup/admin)', function() {
		it('Does not allow setup after admin is created', function(done) {
			api.post('/setup/admin', {
				username: 'test', password: 'test', email: 'unit.test@12150w.com'
			}).then(function() {
				done(new Error('Admin setup should not have succeeded'));
				
			}, function(err) {
				assert.equal(err.msg.statusCode, 400);
				
				done();
			}).done();
		});
	});
	
	describe('Database Setup Endpoint (/setup/database)', function() {
		it('Does not allow database setup after setup', function(done) {
			api.post('/setup/database', {database: 'none'}).then(function() {
				done(new Error('Database setup should not have succeeded'));
			}, function() {
				done();
			}).done();
		});
	});
	
	describe('Setup Status (/setup/status)', function() {
		describe('GET', function() {
			var stepAPI;
			beforeEach('Setup a testing API', function(done) {
				stepAPI = new TestAPI();
				stepAPI.listen().then(function(){done();}, done).done();
			});
			
			it('Loads "complete" status when setup is complete', function(done) {
				api.get('/setup/status').then(function(res) {
					assert.equal(res.body.status, 'complete');
					
					done();
				}, done).done();
			});
			
			it('Loads null status when no database setup', function(done) {
				// Clear the database settings
				stepAPI.settings.set('postgres', undefined).then(function() {
					// Load the status
					return stepAPI.get('/setup/status');
					
				}).then(function(res) {
					assert.equal(res.body.status, null);
					
					done()
				}, done).done();
			});
			
			it('Loads "db" when the database is setup', function(done) {
				stepAPI.get('/setup/status').then(function(res) {
					assert.equal(res.body.status, 'db');
					
					done();
				}, done).done();
			});
		});
	});
});
