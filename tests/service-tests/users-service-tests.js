/* Tests the users service
 */
var assert = require('assert'),
	l2Password = require('../../src/password'),
	TestUtil = require('../../src/test-util'),
	TestAPI = require('../../src/test-api');

describe('Users Service', function() {
	var api = new TestAPI({doSetup: true}),
		existing = {
			username: 'unit-test-1',
			email: 'unit.test1@12150w.com'
		};
	before('Set up the test API', function(done) {
		api.listen().then(function() {done();}, done).done();
	});
	before('Send an existing invitation', function(done) {
		api.securePost('/users/invite', existing).then(function() {
			done();
		}, done).done();
	});


	describe('Invite Endpoint (/users/invite)', function() {
		describe('POST', function() {
			afterEach('Clear mailer event listeners', function() {
				api.mail._eventMap = {};
			});

			it('Requires admin session', TestUtil.verifyRequiresAdmin('/users/invite', 'POST', api));

			it('Requires "username" in body', function(done) {
				api.securePost('/users/invite', {email: 'unit.test@12150w.com'}).then(function() {
					done(new Error('Request should not have succeeded'));
				}, function(err) {
					assert.equal(err.msg.statusCode, 400);

					done();
				}).done();
			});

			it('Requires "email" in body', function(done) {
				api.securePost('/users/invite', {username: 'unittest'}).then(function() {
					done(new Error('Request should not have succeeded'));
				}, function(err) {
					assert.equal(err.msg.statusCode, 400);

					done();
				}).done();
			});

			it('Checks for duplicate users by username', function(done) {
				api.securePost('/users/invite', {
					username: existing.username,
					email: TestUtil.getUniqueEmail()
				}).then(function() {
					done(new Error('Request should not have succeeded'));
				}, function(err) {
					assert.equal(err.msg.statusCode, 400);

					done();
				}).done();
			});

			it('Checks for duplicate users by email', function(done) {
				api.securePost('/users/invite', {
					username: 'username12312',
					email: existing.email
				}).then(function() {
					done(new Error('Request should not have succeeded'));
				}, function(err) {
					assert.equal(err.msg.statusCode, 400);

					done();
				}).done();
			});

			it('Creates a new invitation and user', function(done) {
				var user = {
					username: 'ut-invite-good',
					email: TestUtil.getUniqueEmail()
				};

				api.securePost('/users/invite', user).then(function(res) {
					// Lookup the new invitation
					assert.ok(res.body.activationCode != null, 'No activationCode in response');
					assert.ok(res.body.expiration != null, 'No expiration in response');
					return api.db.query('SELECT * FROM l2_invites WHERE activation_code = $1 LIMIT 1', [
						res.body.activationCode
					]);

				}).then(function(inviteRes) {
					// Lookup the user
					assert.ok(inviteRes.rows.length > 0, 'No invitation found with activation code from response');
					return api.db.query('SELECT * FROM l2_users WHERE id = $1 LIMIT 1', [
						inviteRes.rows[0].user_id
					]);

				}).then(function(userRes) {
					// Verify the user data
					var dbUser = userRes.rows[0];
					assert.equal(dbUser.username, user.username);
					assert.equal(dbUser.email, user.email);
					assert.equal(dbUser.activation_status, 'invitation sent');

				}).then(function() {done();}, done).done();
			});

			it('Sends the activation code to the user in an email', function(done) {
				this.timeout(250);
				var user = {
					username: 'ute-invite-good',
					email: TestUtil.getUniqueEmail()
				};
				var emailBody = null;

				api.mail.on('mail', function(res) {
					emailBody = res.response.toString('utf8');
				});

				api.securePost('/users/invite', user).then(function(res) {
					assert.ok(emailBody != null, 'No email was sent');
					assert.ok(emailBody.indexOf(res.body.activationCode) > -1, 'activation code not in email body');

					done();
				}).done();
			});
		});
	});

	describe('Invite Accept Endpoint (/users/invite/accept/:activation_code)', function() {
		var inviteCode,
			acceptUsername = 'ccpt-nvt-tst';

		beforeEach('Create a test invite', function(done) {
			api.securePost('/users/invite', {
				username: acceptUsername,
				email: 'ccpt.nvt.tst@12150w.com'
			}).then(function(res) {
				// Store the invite code
				inviteCode = res.body.activationCode;

			}).then(function() { done(); }, done).done();
		});
		afterEach('Destroy the test invite', function(done) {
			api.db.query('DELETE FROM l2_users WHERE username = $1',
				[acceptUsername]
			).then(function() { done(); }, done).done();
		});

		describe('POST', function() {
			it('Does not work with a non-existant activation code', function(done) {
				api.post('/users/invite/accept/bad-code', {password: 'test'}).then(function() {
					done(new Error('Accept should not have succeeded'));
				}, function(err) {
					assert.equal(err.msg.statusCode, 404);

					done();
				});
			});

			it('Does not work for expired activations', function(done) {
				var pastDue = new Date();
				pastDue.setMinutes(pastDue.getMinutes() - 1);

				api.db.query('UPDATE l2_invites SET expiration = $1 WHERE activation_code = $2',
					[pastDue, inviteCode]
				).then(function() {
					// Accept the invitation
					return api.post('/users/invite/accept/' + inviteCode, {password: 'test'});

				}).then(function() {
					throw new Error('Accept invite should have failed');

				}, function(err) {
					assert.equal(err.msg.statusCode, 400);

				}).then(function() { done(); }, done).done();
			});

			it('Sets the user password and activation status', function(done) {
				var defaultStatus = 'awaiting approval',
					rawPassword = 'this-is-a-test';

				api.settings.set('users.defaultStatus', defaultStatus).then(function() {
					// Accept the invitation
					return api.post('/users/invite/accept/' + inviteCode, {password: rawPassword});

				}).then(function(res) {
					// Load the user
					return api.db.query('SELECT password, activation_status FROM l2_users WHERE username = $1 LIMIT 1',
						[res.body.username]
					);

				}).then(function(queryRes) {
					// Validate the password and activation status set
					var user = queryRes.rows[0];
					assert.equal(defaultStatus, user.activation_status);
					return l2Password.fromHash(user.password).matches(rawPassword).then(function(matches) {
						assert.ok(matches === true, 'The stored password is incorrectly encrypted or does not match');
					});

				}).then(function() { done(); }, done).done();
			});
		});

		describe('GET', function() {
			it('Loads the invite username', function(done) {
				api.get('/users/invite/accept/' + inviteCode).then(function(res) {
					assert.ok(res.body.username === acceptUsername);

				}).then(function() { done(); }, done).done();
			});
		});
	});

	describe('Invite Accept Endpoint (/users/invite/accept/:activation_code) 2', function() {
		var inviteCodes = [],
			acceptUsernameBase = 'ccpt-nvt-tst',
			acceptEmailBase = 'ccpt.nvt.tst';

		beforeEach('Create a test invites', function(done) {
			api.securePost('/users/invite', {
				username: acceptUsernameBase + 1,
				email: acceptEmailBase + 1 + '@12150w.com'
			}).then(function(res) {
				// Store the invite code
				inviteCodes.push(res.body.activationCode);

			}).then(function() {

				return api.securePost('/users/invite', {
					username: acceptUsernameBase + 2,
					email: acceptEmailBase + 2 + '@12150w.com'
				});

			}).then(function(res) {
				// Store the invite code
				inviteCodes.push(res.body.activationCode);

			}).then(function() { done(); }, done).done();
		});

		afterEach('Destroy the test invite', function(done) {
			api.db.query('DELETE FROM l2_users WHERE username LIKE $1',
				[acceptUsernameBase + '%']
			).then(function() { done(); }, done).done();
		});

		describe('POST', function() {
			var user1;

			it('Sets the user password and activation status', function(done) {
				var defaultStatus = 'awaiting approval',
					rawPassword = 'this-is-a-test',
					otherRawPassword = 'this-is-also-a-test';

				api.settings.set('users.defaultStatus', defaultStatus).then(function() {
					// Accept the 1st invitation
					return api.post('/users/invite/accept/' + inviteCodes[0], {password: rawPassword});

				}).then(function(res) {
					// Load the 1st user
					return api.db.query('SELECT id, password, activation_status FROM l2_users WHERE username = $1 LIMIT 1',
						[res.body.username]
					);

				}).then(function(queryRes) {
					// Store the initial password for user1
					user1 = queryRes.rows[0];

					// Accept the 2nd invitation
					return api.post('/users/invite/accept/' + inviteCodes[1], {password: otherRawPassword});

				}).then(function(res) {

					// Reload the 2nd user
					return api.db.query('SELECT password FROM l2_users WHERE id = $1 LIMIT 1',
						[user1.id]
					);

				}).then(function(queryRes) {
					var updatedUser1 = queryRes.rows[0];

					// Validate that the password has not changed
					assert.equal(user1.password, updatedUser1.password);

				}).then(function() { done(); }, done).done();
			});
		});
	});

	describe('Manage Endpoint (/users/manage)', function() {
		describe('GET', function() {
			it('Requires admin session', TestUtil.verifyRequiresAdmin('/users/manage', 'GET', api));

			it('Returns all users', function(done) {
				return api.secureGet('/users/manage').then(function(res) {
					assert.ok(res.body.users.length > 0);

				}).then(function() {done();}, done).done
			});
		});
	});

	describe('Details Endpoint (/users/details/:username)', function() {
		describe('GET', function() {
			it('Requires admin session', TestUtil.verifyRequiresAdmin(
				'/users/details/' + existing.username, 'GET', api));

			it('Returns 404 for an unknown username in the URL', function(done) {
				api.secureGet('/users/details/not-exists').then(function() {
					done(new Error('Request should not have succeeded'));
				}, function(err) {
					assert.equal(err.msg.statusCode, 404);

					done();
				}).done();
			});

			it('Returns the basic user information', function(done) {
				api.secureGet('/users/details/' + existing.username).then(function(res) {
					assert(res.body.id != null);
					assert.equal(res.body.username, existing.username);
					assert.equal(res.body.email, existing.email);

				}).then(function() {done();}, done).done();
			});
			
			it('Returns membership information', function(done) {
				api.secureGet('/users/details/' + existing.username).then(function(res) {
					assert.ok(res.body.memberships != null);
				}).then(function() {done();}, done).done();
			})
		});
	});

	describe('Update Endpoint (/users/update/:user_id)', function() {
		var userId;
		before('Load the user id', function(done) {
			api.db.query('SELECT id FROM l2_users WHERE username = $1', [existing.username]).then(function(userRes) {
				userId = userRes.rows[0].id;
				done();
			});
		});

		describe('POST', function() {
			var inGroupKey = 'group-is-in',
				notGroupKey = 'group-not-in';
			
			before('Set up groups', function(done) {
				api.db.query(
					'INSERT INTO l2_groups(key) VALUES ($1), ($2)',
					[inGroupKey, notGroupKey]
				).then(function() {
					// Add the user to a group
					return api.db.query(
						'INSERT INTO l2_memberships(user_id, group_key) VALUES ($1, $2)',
						[userId, inGroupKey]
					);
					
				}).then(function() { done(); }, done).done();
			});
			
			after('Delete groups and memberships', function(done) {
				api.db.query(
					'DELETE FROM l2_groups WHERE key = $1 OR key = $2',
					[inGroupKey, notGroupKey]
				).then(function() { done(); }, done).done();
			});
			
			it('Requires admin session', TestUtil.verifyRequiresAdmin(
				'/users/update/' + userId, 'POST', api));

			it('Updates the user', function(done) {
				api.securePost('/users/update/' + userId, {activation_status: 'test1'}).then(function() {
					// Load the updated user
					return api.db.query('SELECT activation_status FROM l2_users WHERE id = $1', [userId]);

				}).then(function(queryRes) {
					var user = queryRes.rows[0];
					assert.equal(user.activation_status, 'test1');

				}).then(function() { done(); }, done).done();
			});
			
			it('Adds memberships', function(done) {
				api.securePost('/users/update/' + userId, {
					memberships: [inGroupKey, notGroupKey]
				}).then(function() {
					// Query the membership
					return api.db.query(
						'SELECT group_key FROM l2_memberships WHERE user_id = $1 AND group_key = $2',
						[userId, notGroupKey]
					);
					
				}).then(function(res) {
					assert.ok(res.rows.length === 1, 'the user was not added to the group');
					
				}).then(function() { done(); }, done).done();
			});
			
			it('Removes memberships', function(done) {
				api.securePost('/users/update/' + userId, {
					memberships: []
				}).then(function() {
					// Query all user memberships
					return api.db.query(
						'SELECT group_key FROM l2_memberships WHERE user_id = $1',
						[userId]
					);
					
				}).then(function(res) {
					assert.ok(res.rows.length === 0, 'the user was not removed from the group');
					
				}).then(function() { done(); }, done).done();
			});
			
			it('Fails with an invalid group key', function(done) {
				api.securePost('/users/update/' + userId, {
					memberships: [notGroupKey + '-invalid']
				}).then(function() {
					throw new Error('The request should have failed');
				}, function(err) {
					if(err.msg.statusCode !== 400) throw err;
					
				}).then(function() { done(); }, done).done();
			});
			
			it('Does not delete groups that are currently saved', function(done) {
				api.securePost('/users/update/' + userId, {
					memberships: [inGroupKey, notGroupKey]
				}).then(function() {
					return api.securePost('/users/update/' + userId, {
						memberships: [inGroupKey, notGroupKey]
					});
					
				}).then(function() {
					// Load the memberships
					return api.db.query(
						'SELECT group_key FROM l2_memberships WHERE user_id = $1',
						[userId]
					);
					
				}).then(function(res) {
					assert.equal(res.rows.length, 2, 'The groups were changed');
					
				}).then(function() { done(); }, done).done();
			});
			
			it('Updates firstname and lastname', function(done) {
				api.db.query(
					'UPDATE l2_users SET firstname = NULL, lastname = NULL WHERE id = $1',
					[userId]
				).then(function() {
					// Update the first and last names
					return api.securePost('/users/update/' + userId, {
						firstname: 'updated-fn',
						lastname: 'updated-ln'
					});
					
				}).then(function() {
					// Lookup the user again
					return api.db.query(
						'SELECT firstname, lastname FROM l2_users WHERE id = $1 LIMIT 1',
						[userId]
					);
					
				}).then(function(res) {
					user = res.rows[0];
					assert.equal(user.firstname, 'updated-fn', 'The firstname was not updated');
					assert.equal(user.lastname, 'updated-ln', 'The lastname was not updated');
					
				}).then(function() { done(); }, done).done();
			});
			
			it('Retains firstname and lastname if not specified', function(done) {
				api.db.query(
					'UPDATE l2_users SET firstname = $2, lastname = $3 WHERE id = $1',
					[userId, 'keep-fn', 'keep-ln']
				).then(function() {
					// Update the first and last names
					return api.securePost('/users/update/' + userId, {});
					
				}).then(function() {
					// Lookup the user again
					return api.db.query(
						'SELECT firstname, lastname FROM l2_users WHERE id = $1 LIMIT 1',
						[userId]
					);
					
				}).then(function(res) {
					user = res.rows[0];
					assert.equal(user.firstname, 'keep-fn', 'The firstname was changed');
					assert.equal(user.lastname, 'keep-ln', 'The lastname was changed');
					
				}).then(function() { done(); }, done).done();
			});
		});
	});

	describe('Load Groups Endpoint (/users/all-groups)', function() {
		var testGroup = 'test-all-group';
		
		before('Create a test group', function(done) {
			api.db.query("INSERT INTO l2_groups(key) VALUES ($1);", [testGroup]).then(function() { done(); }, done).done();
		});
		
		after('Delete the test group', function(done) {
			api.db.query('DELETE FROM l2_groups WHERE key = $1', [testGroup]).then(function() { done(); }, done).done();
		});
		
		it('Requires admin session', TestUtil.verifyRequiresAdmin('/users/all-groups', 'GET', api));
		
		it('Loads all the groups', function(done) {
			api.secureGet('/users/all-groups').then(function(res) {
				assert.equal(res.body.groups[0].key, testGroup);
			}).then(function() { done(); }, done).done();
		});
	});
	
	describe('Create Group Endpoint (/users/create-group)', function() {
		var testGroup = 'test-created-group';
		
		before('Create a test group', function(done) {
			api.db.query("INSERT INTO l2_groups(key) VALUES ($1);", [testGroup]).then(function() { done(); }, done).done();
		});
		
		after('Delete the test group', function(done) {
			api.db.query('DELETE FROM l2_groups WHERE key = $1', [testGroup]).then(function() { done(); }, done).done();
		});
		
		it('Requires admin session', TestUtil.verifyRequiresAdmin(
			'/users/create-group', 'POST', api));
		
		it('Creates a new group', function(done) {
			var testKey = 'successful-new-key';
			
			api.securePost('/users/create-group', {
				key: testKey
			}).then(function(res) {
				// Look up the created group
				return api.db.query('SELECT key FROM l2_groups WHERE key = $1', [testKey]);
				
			}).then(function(groupRes) {
				assert.equal(groupRes.rows[0].key, testKey);
				
			}).then(function() { done(); }, done).done();
		});
		
		it('Stops if a group with the same key exists', function(done) {
			api.securePost('/users/create-group', {
				key: testGroup
			}).then(function() {
				throw new Error('The request should have failed');
			}, function(err) {
				if(err.msg.statusCode !== 400) throw err;
			}).then(function() { done(); }, done).done();
		});
	});
	
	describe('Group Delete Endpoint (/users/delete-group/:group-key)', function(done) {
		var testGroup = 'test-delete-group';
		
		beforeEach('Create a test group', function(done) {
			api.db.query("INSERT INTO l2_groups(key) VALUES ($1);", [testGroup]).then(function() { done(); }, done).done();
		});
		
		afterEach('Delete the test group', function(done) {
			api.db.query('DELETE FROM l2_groups WHERE key = $1', [testGroup]).then(function() { done(); }, done).done();
		});
		
		it('Requires admin session', TestUtil.verifyRequiresAdmin(
			'/users/delete-group', 'POST', api));
		
		it('Deletes the group from the database', function(done) {
			api.securePost('/users/delete-group', {
				key: testGroup
				
			}).then(function() {
				// Look up the group
				return api.db.query('SELECT key FROM l2_groups WHERE key = $1', [testGroup]);
				
			}).then(function(delRes) {
				assert.ok(delRes.rows.length < 1);
				
			}).then(function() { done(); }, done).done();
		});
	});
	
});
