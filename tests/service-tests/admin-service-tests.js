var TestAPI = require('../../src/test-api'),
	assert = require('assert');

describe('Admin Service', function() {
	var api = new TestAPI({doSetup: true});
	before('Set up the test API', function(done) {
		api.listen().then(function() {done();}, done).done();
	});
	
	describe('Login Endpoint (/admin-api/login)', function() {
		it('Fails with an invalid password', function(done) {
			api.post('/admin-api/login', {
				username: 'test', password: 'invalid-password'
			}).then(function() {
				done(new Error('Login should have failed'));
			}, function(err) {
				assert.equal(err.msg.statusCode, 401);
				
				done();
			}).done();
		});
		
		it('Fails with an invalid username', function(done) {
			api.post('/admin-api/login', {
				username: 'invalid-username', password: 'test'
			}).then(function() {
				done(new Error('Login should have failed'));
			}, function(err) {
				assert.equal(err.msg.statusCode, 401);
				
				done();
			}).done();
		});
		
		it('Succeeds with valid credentials', function(done) {
			api.post('/admin-api/login', {
				username: 'test', password: 'test'
			}).then(function(res) {
				assert.ok(res.body.sid != null);
				
				done();
			}, done).done();
		});
	});
});
