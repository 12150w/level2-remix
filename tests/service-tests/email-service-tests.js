/* Tests the email service
 */
var TestAPI = require('../../src/test-api'),
	TestUtil = require('../../src/test-util');
	assert = require('assert');

describe('Setup Service', function() {
	var api = new TestAPI({doSetup: true});
	before('Set up the test API', function(done) {
		api.listen().then(function() {done();}, done).done();
	});

    describe('Settings Endpoint (/email/settings)', function() {
        beforeEach('Set the default mail settings', function(done) {
            api.settings.set('email', {
                transport: 'debug',
                options: null
            }).then(function() { done(); }, done).done();
        });

        describe('GET', function() {
            it('loads the current settings', function(done) {
                api.settings.set('email.transport', 'testing123').then(function() {
                    // Load the settings
                    return api.secureGet('/email/settings');

                }).then(function(res) {
                    assert.equal(res.body.transport, 'testing123');

                }).then(function() { done(); }, done).done();
            });

            it('Requires admin session', TestUtil.verifyRequiresAdmin('/email/settings', 'GET', api));
        });

        describe('POST', function() {
            it('Updates the email settings', function(done) {
                api.securePost('/email/settings', {
                    transport: 'smtp',
                    options: {
                        username: 'test',
                        host: '127.0.0.1'
                    }
                }).then(function() {
                    // Load the saved settings
                    return api.settings.get('email');

                }).then(function(emailSettings) {
                    assert.equal(emailSettings.transport, 'smtp');
                    assert.equal(emailSettings.options.username, 'test');
                    assert.equal(emailSettings.options.host, '127.0.0.1');

                }).then(function() { done(); }, done).done();
            });

            it('Requires admin session', TestUtil.verifyRequiresAdmin('/email/settings', 'POST', api));
        });
    });

});
