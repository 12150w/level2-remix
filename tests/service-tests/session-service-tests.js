/*
 * Session Service Tests
 *
 */
var TestAPI = require('../../src/test-api'),
	TestUtil = require('../../src/test-util'),
	assert = require('assert'),
	validateUserBase = require('../../src/services/req-context')._validateUser_;

describe('Session Service', function() {
	var api = new TestAPI({doSetup: true}),
		testUser = {
			username: 's-test',
			password: 'test',
			email: 's.test@12150w.com'
		};

	before('Set up the test API', function(done) {
		api.listen().then(function() {done();}, done).done();
	});

	afterEach('Remove any sessions', function(done) {
		api.db.query('DELETE FROM l2_sessions;').then(function() {done();}, done).done();
	});

	before('Set up a user to test with', function(done) {
		// Send the invitation
		api.securePost('/users/invite', {
			username: testUser.username,
			email: testUser.email

		}).then(function(res) {
			// Accept the invitation
			return api.post('/users/invite/accept/' + res.body.activationCode, {
				password: testUser.password
			});

		}).then(function(res) {
			// Verify the user is active
			return api.db.query('UPDATE l2_users SET activation_status = NULL WHERE username = $1', [testUser.username]);

		}).then(function() {
			// Get the user's id
			return api.db.query('SELECT id FROM l2_users WHERE username = $1 LIMIT 1', [testUser.username]).then(function(res) {
				testUser.id = res.rows[0].id;
			});

		}).then(function() { done(); }, done).done();
	});

	describe('Login Endpoint (/session/login)', function() {
		beforeEach('Activate the user', function(done) {
			api.db.query(
				'UPDATE l2_users SET activation_status = NULL WHERE id = $1',
				[testUser.id]
			).then(function() { done(); }, done).done();
		});

		describe('POST', function() {
			it('Fails with an invalid username', function(done) {
				api.post('/session/login', {
					username: testUser.username + '_not',
					password: testUser.password
				}).then(function() {
					throw new Error('The login should not have succeeded');
				}, function(err) {
					if(err.msg.statusCode !== 401) throw err;
				}).then(function() { done(); }, done).done();
			});

			it('Fails with an invalid password', function(done) {
				api.post('/session/login', {
					username: testUser.username,
					password: testUser.password + '_not'
				}).then(function() {
					throw new Error('The login should not have succeeded');
				}, function(err) {
					if(err.msg.statusCode !== 401) throw err;
				}).then(function() { done(); }, done).done();
			});

			it('Fails with an inactive user', function(done) {
				// De-activate the user
				api.db.query(
					"UPDATE l2_users SET activation_status = 'banned' WHERE id = $1",
					[testUser.id]
				).then(function() {
					// Try to log in
					return api.post('/session/login', {
						username: testUser.username,
						password: testUser.password
					});

				}).then(function() {
					throw new Error('The login attempt should have failed');
				}, function(err) {
					if(err.msg.statusCode !== 401) throw err;
					assert.ok(err.message.indexOf('banned') > -1, 'The activation status was not in the error message');
				}).then(function() { done(); }, done).done();
			});
            
            it('Fails with a non-setup (no password) user', function(done) {
                // Create an inactive user
                api.securePost('/users/invite', {
                    username: 'no-pw-inactive',
                    email: 'test.no.pw.inactive@12150w.com'
                }).then(function() {
                    // Try to login as the user
                    return api.post('/session/login', {
                        username: 'no-pw-inactive',
                        password: 'test'
                    });
                    
                }).then(function() {
                    throw new Error('The login attempt should fail');
                    
                }, function(err) {
                    if(err.msg.statusCode !== 401) throw err;
                    
                }).then(function() { done(); }, done).done();
            });

			it('Creates a valid session with valid credentials', function(done) {
				api.post('/session/login', {
					username: testUser.username,
					password: testUser.password
				}).then(function(res) {
					// Query the valid session
					assert.ok(!!res.body.sid, 'No sid in response to login');
					return api.db.query('SELECT user_id FROM l2_sessions WHERE sid = $1 AND LOCALTIMESTAMP < expires', [res.body.sid]);

				}).then(function(sessionRes) {
					// Validate the session is active
					assert.ok(sessionRes.rows.length > 0, 'No valid session with sid from login was found');

				}).then(function() { done(); }, done).done();
			});

			it('Re-uses an open session', function(done) {
				var initialSid;

				// Initial login
				api.post('/session/login', {
					username: testUser.username,
					password: testUser.password
				}).then(function(res) {
					// Login again
					initialSid = res.body.sid;
					return api.post('/session/login', {
						username: testUser.username,
						password: testUser.password
					});

				}).then(function(res) {
					// Verify the same sid was used
					assert.equal(res.body.sid, initialSid, 'The same sid was not used');

				}).then(function() { done(); }, done).done();
			});

			it('Creates a new session after existing expires', function(done) {
				var firstSid;

				api.post('/session/login', {
					username: testUser.username,
					password: testUser.password
				}).then(function(res) {
					// Close the new session
					firstSid = res.body.sid;
					return api.request({
						method: 'POST',
						url: '/session/logout',
						headers: { 'Authorization': firstSid }
					});

				}).then(function() {
					// Login again
					return api.post('/session/login', {
						username: testUser.username,
						password: testUser.password
					});

				}).then(function(res) {
					// Verify a new session was created
					assert.ok(res.body.sid !== firstSid, 'The same session was used after it was closed');

				}).then(function() { done(); }, done).done();
			});
		});
	});

	describe('Logout Endpoint (/session/logout)', function() {
		var logoutSid;
		beforeEach('Create a valid session to work with', function(done) {
			api.post('/session/login', {
				username: testUser.username,
				password: testUser.password
			}).then(function(res) {
				logoutSid = res.body.sid;

			}).then(function() { done(); }, done).done();
		});

		describe('POST', function() {
			it('Closes the session', function(done) {
				api.request({
					method: 'POST',
					url: '/session/logout',
					headers: { 'Authorization': logoutSid }
				}).then(function() {
					// Find the closed session
					return api.db.query('SELECT expires FROM l2_sessions WHERE sid = $1', [logoutSid]);

				}).then(function(sessionRes) {
					// Verify that the session is closed
					return api.db.query('SELECT LOCALTIMESTAMP AS dbtime').then(function(timeRes) {
						assert.ok(sessionRes.rows[0].expires <= timeRes.rows[0].dbtime);
					});

				}).then(function() { done(); }, done).done();
			});

			it('Does not fail for an already closed session', function(done) {
				api.request({
					method: 'POST',
					url: '/session/logout',
					headers: { 'Authorization': logoutSid }
				}).then(function() {
					// Close the closed session
					return api.request({
						method: 'POST',
						url: '/session/logout',
						headers: { 'Authorization': logoutSid }
					});

				}).then(function() { done(); }, done).done();
			});
		});
	});

	describe('res.l2.validateUser', function() {
		var validSid, validateUser, req,
			testGroupKey = 'testing-123';

		beforeEach('Create a valid session to work with', function(done) {
			api.post('/session/login', {
				username: testUser.username,
				password: testUser.password
			}).then(function(res) {
				validSid = res.body.sid;

			}).then(function() { done(); }, done).done();
		});

		beforeEach('Set up the fake request', function() {
			req = {
				_headers: {},
				get: function(key) {
					return this._headers[key];
				}
			};
			validateUser = validateUserBase.bind({api: api, req: req});
		});

		afterEach('Remove the user from any groups', function(done) {
			api.db.query(
				'DELETE FROM l2_memberships WHERE user_id = $1',
				[testUser.id]
			).then(function() { done(); }, done).done();
		});

		before('Create a test group', function(done) {
			api.db.query(
				'INSERT INTO l2_groups (key) VALUES ($1)',
				[testGroupKey]
			).then(function() { done(); }, done).done();
		});

		it('Fails for an invalid SID', function(done) {
			req._headers['Authorization'] = validSid + '_not';

			validateUser().then(function() {
				throw new Error('validate should have failed');
			}, function(err) {
				if(err.httpStatus !== 401) throw err;
			}).then(function() { done(); }, done).done();
		});

		it('Fails for a closed session SID', function(done) {
			req._headers['Authorization'] = validSid;

			// Logout
			api.request({
				method: 'POST',
				url: '/session/logout',
				headers: { 'Authorization': validSid }
			}).then(function() {
				// Validate
				return validateUser();

			}).then(function() {
				throw new Error('validate should have failed');
			}, function(err) {
				if(err.httpStatus !== 401) throw err;
			}).then(function() { done(); }, done).done();
		});

		it('Succeeds with valid sid and no groups', function(done) {
			req._headers['Authorization'] = validSid;

			validateUser().then(function(userId) {
				assert.ok(!!userId, 'No user id was resolved from validate');

			}).then(function() { done(); }, done).done();
		});

		it('Fails if the user is not in the given group', function(done) {
			req._headers['Authorization'] = validSid;

			validateUser(testGroupKey).then(function() {
				throw new Error('validate should have failed');
			}, function(err) {
				if(err.httpStatus !== 401) throw err;
			}).then(function() { done(); }, done).done();
		});

		it('Succeeds if the sid is valid and the user is a member of one group', function(done) {
			req._headers['Authorization'] = validSid;

			// Add the user to the test group
			api.db.query(
				'INSERT INTO l2_memberships (user_id, group_key) VALUES ($1, $2)',
				[testUser.id, testGroupKey]

			).then(function() {
				// Validate the user
				return validateUser(['some-other-group', testGroupKey]);

			}).then(function() { done(); }, done).done();
		});
		
		it('Fails if the sid is expired', function(done) {
			api.db.query(
				"UPDATE l2_sessions SET expires = LOCALTIMESTAMP - interval '1 second' WHERE sid = $1",
				[validSid]
			
			).then(function() {
				req._headers['Authorization'] = validSid;
				
				return validateUser().then(function() {
					throw new Error('validate should have failed');
				}, function(err) {
					if(err.httpStatus !== 401) throw err;
				});
				
			}).then(function() { done(); }, done).done();
		});
	});

});
