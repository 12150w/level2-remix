/*
 * Data Service Tests
 *    Tests the data service and its functionality
 *
 */
var TestAPI = require('../../src/test-api'),
	TestUtil = require('../../src/test-util'),
	assert = require('assert'),
	path = require('path');

describe('Data Service', function() {
	var api = new TestAPI({doSetup: true});

	before('Start the test API', function(done) {
		api.db.setMigrationPath(path.join(__dirname, 'test-db'));
		api.exposeCollection('contacts');
		api.listen().then(function() { done(); }, done).done();
	});


	describe('Record Creation', function() {
		it('Requires user session', TestUtil.verifyRequiresSession('/data/contacts', 'POST', api));

		it('Inserts a record into the collection', function(done) {
			api.sessionPost('/data/contacts', {
				name: 'Joey',
				id_number: 12
			}).then(function(res) {
				assert.ok(res.body.id != null, 'id was blank in the response');
				return api.db.query('SELECT name FROM contacts WHERE id = $1', [res.body.id]);

			}).then(function(res) {
				assert.ok(res.rows.length === 1, 'The record was not inserted');
				assert.ok(res.rows[0].name === 'Joey', 'The inserted record did not have the correct data');

			}).then(function() { done(); }, done).done();
		});

		it('Does not allow access to restricted collections', function(done) {
			api.sessionPost('/data/l2_users', {
				username: 'invalid'
			}).then(function() {
				throw new Error('Restricted collections should not be accessible');
			}, function(err) {
				assert.ok(err.msg.statusCode === 400);
			}).then(function() { done(); }, done).done();
		});
	});


	describe('Finding Records by Id', function() {
		var testContact;

		before('Create a test contact', function(done) {
			api.sessionPost('/data/contacts', {
				name: 'Test Contact'
			}).then(function(res) {
				res.body.data.id = res.body.id;
				testContact = res.body.data;
			}).then(function() { done(); }, done).done();
		});

		after('Remove the test contact', function(done) {
			api.db.query('DELETE FROM contacts').then(function() { done(); }, done).done();
		});

		it('Requires user session', TestUtil.verifyRequiresSession('/data/contacts/0', 'GET', api));

		it('Returns the matching record', function(done) {
			api.sessionGet('/data/contacts/' + testContact.id).then(function(res) {
				assert.ok(res.body.id === testContact.id, 'The returned id does not match');
				assert.ok(res.body.data.name === 'Test Contact', 'The data was not properly returned');

			}).then(function() { done(); }, done).done();
		});

		it('Returns an empty result when the record is not found', function(done) {
			api.sessionGet('/data/contacts/1' + testContact.id).then(function(res) {
				assert.ok(JSON.stringify(res.body) === '{}', 'The response was not an empty object');

			}).then(function() { done(); }, done).done();
		});

		it('Does not allow access to restricted collections', function(done) {
			api.sessionGet('/data/l2_users/1').then(function() {
				throw new Error('Restricted collections should not be accessible');
			}, function(err) {
				assert.ok(err.msg.statusCode === 400);
			}).then(function() { done(); }, done).done();
		});
	});


	describe('Querying Records', function() {
		var testContacts = [];

		before('Create test contacts', function(done) {
			api.sessionPost('/data/contacts', {
				name: 'Test 1'
			}).then(function(res) {
				res.body.data.id = res.body.id;
				testContacts.push(res.body.data);
				return api.sessionPost('/data/contacts', {
					name: 'Test 2'
				});

			}).then(function(res) {
				res.body.data.id = res.body.id;
				testContacts.push(res.body.data);

			}).then(function() { done(); }, done).done();
		});

		after('Remove the test contact', function(done) {
			api.db.query('DELETE FROM contacts').then(function() { done(); }, done).done();
		});

		it('Requires user session', TestUtil.verifyRequiresSession('/data/query/contacts', 'POST', api));

		it('Returns records matching the query', function(done) {
			api.sessionPost('/data/query/contacts', {
				id: [testContacts[0].id, testContacts[1].id]
			}).then(function(res) {
				assert.ok(res.body.records[0].id === testContacts[0].id);
				assert.ok(res.body.records[1].id === testContacts[1].id);
				assert.ok(res.body.records[0].data.name === testContacts[0].name);
				assert.ok(res.body.records[1].data.name === testContacts[1].name);

			}).then(function() { done(); }, done).done();
		});

		it('Returns empty with no matching results', function(done) {
			api.sessionPost('/data/query/contacts', {
				name: testContacts[0].name + '_not'
			}).then(function(res) {
				assert.ok(res.body.records.length === 0);

			}).then(function() { done(); }, done).done();
		});

		it('Does not allow access to restricted collections', function(done) {
			api.sessionPost('/data/query/l2_users', {
				id: 1
			}).then(function() {
				throw new Error('Restricted collections should not be accessible');
			}, function(err) {
				assert.ok(err.msg.statusCode === 400);
			}).then(function() { done(); }, done).done();
		});
	});


	describe('Query SQL Conversion', function() {
		it('Sets key value pairs to EQUAL compare', function() {
			var data = api.db.store._buildQuerySQL({
				testField: 'test value'
			});

			assert.equal(data.sql, '(testField = $1) ', 'The sql is incorrect');
			assert.equal(data.params[0], 'test value', 'The params are incorrect');
		});

		it('Sets key array pairs to IN compare', function() {
			var data = api.db.store._buildQuerySQL({
				testField: ['val 1', 'val 2']
			});

			assert.equal(data.sql, '(testField IN ($1, $2)) ', 'The sql is incorrect');
			assert.equal(data.params[0], 'val 1', 'The first param is incorrect');
			assert.equal(data.params[1], 'val 2', 'The second param is incorrect');
		});

		it('Combines with AND by default', function() {
			var data = api.db.store._buildQuerySQL({
				field1: 'val1',
				field2: ['val2', 'val3']
			});

			assert.equal(data.sql, '(field1 = $1 AND field2 IN ($2, $3)) ', 'The sql is incorrect');
			assert.equal(data.params.length, 3, 'The number of params is incorrect');
		});
	});


	describe('Updating Records', function() {
		var testContact;

		before('Create a test contact', function(done) {
			api.sessionPost('/data/contacts', {
				name: 'Test Contact'
			}).then(function(res) {
				res.body.data.id = res.body.id;
				testContact = res.body.data;
			}).then(function() { done(); }, done).done();
		});

		after('Remove the test contact', function(done) {
			api.db.query('DELETE FROM contacts').then(function() { done(); }, done).done();
		});

		it('Requires user session', TestUtil.verifyRequiresSession('/data/contacts/0', 'PUT', api));

		it('Updates the database record', function(done) {
			api.sessionPut('/data/contacts/' + testContact.id, {
				name: 'DB Name'
			}).then(function() {
				// Re-query the record
				return api.db.query('SELECT name FROM contacts WHERE id = $1', [testContact.id]);

			}).then(function(res) {
				assert.ok(res.rows[0].name === 'DB Name', 'The record was not updated');

			}).then(function() { done(); }, done).done();
		});
		
		it('Updates multiple fields at once', function(done) {
			api.sessionPut('/data/contacts/' + testContact.id, {
				name: 'DB Name',
				id_number: 13
			}).then(function() {
				// Re-query the record
				return api.db.query('SELECT name, id_number FROM contacts WHERE id = $1', [testContact.id]);

			}).then(function(res) {
				assert.ok(res.rows[0].name === 'DB Name', 'The record was not updated');
				assert.ok(res.rows[0].id_number === 13, 'The record was not updated');

			}).then(function() { done(); }, done).done();
		});

		it('Returns the record that was updated', function(done) {
			api.sessionPut('/data/contacts/' + testContact.id, {
				name: 'Return Name'
			}).then(function(res) {
				assert.equal(res.body.id, testContact.id, 'The returned id was incorrect');
				assert.equal(res.body.data.name, 'Return Name', 'The returned data was incorrect');

			}).then(function() { done(); }, done).done();
		});

		it('Does not allow access to restricted collections', function(done) {
			api.sessionPut('/data/l2_users/1', {
				username: 'bad-id'
			}).then(function() {
				throw new Error('Restricted collections should not be accessible');
			}, function(err) {
				assert.ok(err.msg.statusCode === 400);
			}).then(function() { done(); }, done).done();
		});
	});


	describe('Record Deletion', function() {
		var testContact;

		before('Create a test contact', function(done) {
			api.sessionPost('/data/contacts', {
				name: 'Test Contact'
			}).then(function(res) {
				res.body.data.id = res.body.id;
				testContact = res.body.data;
			}).then(function() { done(); }, done).done();
		});

		after('Remove the test contact', function(done) {
			api.db.query('DELETE FROM contacts').then(function() { done(); }, done).done();
		});

		it('Requires user session', TestUtil.verifyRequiresSession('/data/contacts/0', 'DELETE', api));

		it('Removes the record from the database', function(done) {
			api.sessionDelete('/data/contacts/' + testContact.id, {}).then(function() {
				return api.db.query('SELECT id FROM contacts WHERE id = $1', [testContact.id]);

			}).then(function(res) {
				assert.ok(res.rows.length === 0, 'The record was not deleted');

			}).then(function() { done(); }, done).done();
		});

		it('Does not allow access to restricted collections', function(done) {
			api.sessionDelete('/data/l2_users/1', {}).then(function() {
				throw new Error('Restricted collections should not be accessible');
			}, function(err) {
				assert.ok(err.msg.statusCode === 400);
			}).then(function() { done(); }, done).done();
		});
	});

});
