/* Tests the users service
 */
var assert = require('assert'),
	Password = require('../../src/password'),
	TestAPI = require('../../src/test-api');

describe('Account Service', function() {
	var api = new TestAPI({doSetup: true}),
		existing = {
			username: 'unit-test-2',
			email: 'unit.test2@12150w.com',
			password: 'unit-test-2'
		};
	before('Set up the test API', function(done) {
		api.listen().then(function() {done();}, done).done();
	});
	before('Create an existing user', function(done) {
		api.securePost('/users/invite', existing).then(function(res) {
			return api.post('/users/invite/accept/' + res.body.activationCode, {
				password: existing.password
			});
			
		}).then(function() { done(); }, done).done();
	});
	
	beforeEach('Clear all PRRs', function(done) {
		api.db.query(
			'DELETE FROM l2_prr'
		).then(function() { done(); }, done).done();
	});
	
	describe('PRR Request Endpoint (/account/prr/request)', function() {
		it('Creates a valid PRR record', function(done) {
			api.post('/account/prr/request', {
				username: existing.username
				
			}).then(function() {
				// Find the new PRR
				return api.db.query('SELECT code, status, expiration FROM l2_prr WHERE LOCALTIMESTAMP < expiration LIMIT 1');
				
			}).then(function(prrRes) {
				var prr = prrRes.rows[0];
				assert.ok(prr.status === null);
				
			}).then(function() { done(); }, done).done();
		});
		
		it('Does not allow multiple active PRRs', function(done) {
			api.post('/account/prr/request', {
				username: existing.username
				
			}).then(function() {
				// Create another PRR
				return api.post('/account/prr/request', {
					username: existing.username
				}).then(function() {
					throw new Error('The request should have failed');
				}, function(err) {
					if(err.msg.statusCode !== 400) throw err;
				});
				
			}).then(function() { done(); }, done).done();
		});
		
		it('Emails the PRR code', function(done) {
			var hadError = null;
			
			api.mail.on('mail', function(data) {
				if(hadError != null) throw hadError;
				
				api.db.query('SELECT code, status, expiration FROM l2_prr LIMIT 1').then(function(prrRes) {
					// Verify the email contains the PRR code
					var prr = prrRes.rows[0],
						resText = data.response.toString('utf8');
					
					assert.ok(resText.indexOf(prr.code) > -1);
					
					// Reset the event map
					api.mail._eventMap = {};
					
				}).then(function() { done(); }, done).done();
			});
			
			api.post('/account/prr/request', {
				username: existing.username
			}).fail(function(err) {
				hadError = err;
			}).done();
		});
	});
	
	describe('PRR Execute Endpoint (/account/prr/execute)', function() {
		it('Updates the user password with a valid PRR code', function(done) {
			api.post('/account/prr/request', {username: existing.username}).then(function() {
				// Find the PRR code
				return api.db.query('SELECT code FROM l2_prr LIMIT 1').then(function(findRes) {
					return findRes.rows[0].code;
				});
				
			}).then(function(prrCode) {
				// Execute the PRR
				return api.post('/account/prr/execute', {
					code: prrCode,
					password: existing.password + 'aha'
				}).then(function() {
					return api.db.query('SELECT password FROM l2_users WHERE username = $1 LIMIT 1', [existing.username]);
				}).then(function(userRes) {
					return userRes.rows[0].password;
				});
				
			}).then(function(newPwHash) {
				// Verify that the password was updated
				var newPw = Password.fromHash(newPwHash);
				return newPw.matches(existing.password + 'aha').then(function(passwordUpdated) {
					if(passwordUpdated !== true) {
						throw new Error('The updated password does not match the PRR password');
					}
				});
				
			}).then(function() { done(); }, done).done();
		});
		
		it('De-activates the used PRR', function(done) {
			var prrCode;
			
			api.post('/account/prr/request', {username: existing.username}).then(function() {
				// Find the PRR code
				return api.db.query('SELECT code FROM l2_prr LIMIT 1').then(function(findRes) {
					prrCode = findRes.rows[0].code;
				});
				
			}).then(function() {
				// Execute the PRR
				return api.post('/account/prr/execute', {
					code: prrCode,
					password: existing.password + 'aha'
				});
				
			}).then(function() {
				// Execute the PRR again
				return api.post('/account/prr/execute', {
					code: prrCode,
					password: existing.password + 'aha2'
				}).then(function() {
					throw new Error('The 2nd PRR execute should have failed');
				}, function(err) {
					if(err.msg.statusCode !== 400) throw err;
				});
				
			}).then(function() { done(); }, done).done();
		});
		
		it('Does not work for expired PRR codes', function(done) {
			var prrCode;
			
			api.post('/account/prr/request', {username: existing.username}).then(function() {
				// Find the PRR code
				return api.db.query('SELECT code FROM l2_prr LIMIT 1').then(function(findRes) {
					prrCode = findRes.rows[0].code;
				});
				
			}).then(function() {
				// Expire the PRR
				return api.db.query("UPDATE l2_prr SET expiration = LOCALTIMESTAMP - interval '1 min' WHERE code = $1", [
					prrCode
				]);
				
			}).then(function() {
				// Execute the PRR
				return api.post('/account/prr/execute', {
					code: prrCode,
					password: existing.password + 'aha2'
				}).then(function() {
					throw new Error('The PRR execute should have failed');
				}, function(err) {
					if(err.msg.statusCode !== 400) throw err;
				});
				
			}).then(function() { done(); }, done).done();
		});
	});
	
});
