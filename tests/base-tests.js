/*
 * Tests the base module
 *
 */
var assert = require('assert'),
	level2 = require('../src');

describe('Class', function() {
	var Parent;

	before('Create sample parent class', function() {
		Parent = level2.base.Class.extend({
			parentAction: function() {
				return true;
			}
		});
	});

	describe('#extend', function() {
		it('Copies parent methods over to the child', function() {
			var Child = Parent.extend({}),
				someKid = new Child();

			assert(someKid.parentAction() === true, 'Parent properties should be copied to child');
		});

		it('Provides override access to super property', function() {
			var OverrideChild = Parent.extend({
				parentAction: function() {
					return this._super();
				}
			});
			var someKid = new OverrideChild();

			assert(someKid.parentAction() === true, 'Overridden functions should have access to parent functions via _super');
		});

		it('Overwrites child properties', function() {
			var OverrideChild = Parent.extend({
				parentAction: function() {
					return false;
				}
			});
			var someKid = new OverrideChild();

			assert(someKid.parentAction() === false, 'Extend should overwrite parent properties with the child properties');
		});

		it('Allows typeof to be accurate', function() {
			var Child = Parent.extend(),
				Other = Parent.extend(),
				instance = new Child();

			assert.ok(instance instanceof Child, 'instanceof should work on the low class');
			assert.ok(instance instanceof Parent, 'instanceof should work on high class');
			assert.ok(instance instanceof Other === false, 'instanceof should not work on random class');
		});

		it('Includes in the extended properties', function() {
			var Child = Parent.extend({
					_someOtherFunction: function() {
						return true;
					}
				}),
				instance = new Child();

			assert.ok(instance._someOtherFunction(), 'The attribute of the extended class was not copied to the prototype');
		});

		describe('Mixins', function() {
			var SampleMixin;
			before('Create sample mixin', function() {
				SampleMixin = level2.base.Class.extend({
					mixinMethod: function() {
						return true;
					}
				});
			});

			it('Copies in attributes from mixins', function() {
				var Child = Parent.extend(SampleMixin, {
					childMethod: function() {
						return true;
					}
				});
				var someKid = new Child();

				assert(someKid.childMethod() === true, 'The child methods should be included as well as the mixin');
				assert(someKid.mixinMethod() === true, 'The mixin methods should be in the extended class');
			});

			it('Overwrites child attributes from mixins', function() {
				var Child = Parent.extend(SampleMixin, {
					mixinMethod: function() {
						return false;
					}
				});
				var someKid = new Child();

				assert(someKid.mixinMethod() === false, 'The child properties should override the mixin properties');
			});

			it('Does not create instance of mixin', function() {
				var Child = Parent.extend(SampleMixin, {});
				var someKid = new Child();

				assert.ok(someKid instanceof SampleMixin === false);
				assert.ok(someKid instanceof Child === true);
			});
		});
	});
});

describe('EventEmitter', function() {
	var TestEmitter = level2.base.Class.extend(level2.base.EventEmitter, {
		sendTestEvent: function() {
			this.emit('test');
		},

		sendTestData: function(data) {
			this.emit('test', data);
		}
	});
	var emitter;
	beforeEach('Create a new emitter implementation', function() {
		emitter = new TestEmitter();
	});

	describe('#on', function() {
		it('Adds an event listener', function(done) {
			this.timeout(250);

			emitter.on('test', function() {
				done();
			});

			emitter.sendTestEvent();
		});

		it('Passes the data to the handlers', function(done) {
			this.timeout(250);
			var testData = {};

			emitter.on('test', function(passedData) {
				assert.ok(passedData === testData);
				done();
			});

			emitter.sendTestData(testData);
		});
	});
});
