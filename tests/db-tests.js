/*
 * Database Tests
 *
 */
var TestAPI = require('../src/test-api'),
	assert = require('assert'),
	path = require('path');

describe('Database', function() {

	describe('#migrate', function() {
		it('Migrates external migrations', function(done) {
			var api = new TestAPI({doSetup: true});
			api.db.setMigrationPath(path.join(__dirname, 'test-migrations'));

			api.listen().then(function() {
				// Verify the external migrations ran
				return api.db.query('SELECT test_val FROM unit_test');

			}).then(function() {
				// Verify the index was updated
				return api.settings.get('externalMigrationIndex').then(function(savedIndex) {
					assert.equal(savedIndex, 1, 'The external index was not set');
				});

			}).then(function() { done(); }, done).done();
		});
	});
});
