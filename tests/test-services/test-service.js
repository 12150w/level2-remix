var assert = require('assert');

module.exports = function(router, api) {
	router.get('/test', function(req, res) {
		assert.ok(res.l2._externalService === true, 'The request context was not set up properly');
		res.status(200).json({});
	});
};
