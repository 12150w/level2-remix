/* 
 * Tests the API used for testing
 * 
 */
var TestAPI = require('../src/test-api'),
	assert = require('assert'),
	fs = require('fs'),
	path = require('path'),
	os = require('os'),
	q = require('q');

describe('TestAPI', function() {
	var setupApi;
	
	before('Start a TestAPI', function(done) {
		setupApi = new TestAPI({doSetup: true});
		return setupApi.listen().then(function() {done();}, done).done();
	});
	
	describe('#listen', function() {
		it('Starts on a socket', function(done) {
			var api = new TestAPI();
			
			api.listen().then(function() {
				assert.ok(api.path != null);
				
				// Verify that a socket was created
				return q.ninvoke(fs, 'stat', api.path);
			}).then(function(sockStats) {
				assert.ok(sockStats.isSocket(), 'No socket was created at api.path');
				
			}).then(done, done).done();
		});
		
		it('Allows calling start multiple times', function(done) {
			var api = new TestAPI({doSetup: true});
			
			api.listen().then(function() {
				return api.listen();
				
			}).then(function() {done();}, done).done();
		});
	});
	
	describe('#request', function() {
		it('Sends requests to the test API', function(done) {
			setupApi.request({uri: '/'}).then(function(res) {
				assert.ok(res.msg.statusCode === 200);
				
			}).then(function() {done();}, done).done();
		});
	});
	
	describe('#get', function() {
		it('Sends a GET request to the API', function(done) {
			setupApi.get('/').then(function(res) {
				assert.ok(res.msg.statusCode === 200);
				
			}).then(function() {done();}, done).done()
		});
	});
	
	describe('#logMessage', function() {
		var testMessage = 'This is a test message',
			testSource = 'UNITTEST';
		
		it('Writes a line to the log file', function(done) {
			var testLogPath = path.join(os.tmpdir(), 'api-log-msg-test.txt'),
				logAPI = new TestAPI({logPath: testLogPath});
			
			logAPI.logMessage(testMessage).then(function() {
				// Read the log
				return q.nfcall(fs.readFile, testLogPath, {encoding: 'utf8'});
				
			}).then(function(logData) {
				// Verify the log saved
				assert.equal(logData, testMessage + os.EOL);
				
			}).then(function() {done();}, done).done();
		});
		
		it('Logs the source in the log message', function(done) {
			var testLogPath = path.join(os.tmpdir(), 'api-log-src-test.txt'),
				logAPI = new TestAPI({logPath: testLogPath});
			
			logAPI.logMessage(testMessage, testSource).then(function() {
				// Read the log
				return q.nfcall(fs.readFile, testLogPath, {encoding: 'utf8'});
				
			}).then(function(logData) {
				// Verify the source saved
				assert.equal(logData, '[' + testSource + '] ' + testMessage + os.EOL);
				
			}).then(function() {done();}, done).done();
		})
	});
	
	describe('#logError', function() {
		var testMessage = 'This is a sample error';
		
		it('Adds the error tag in the line', function(done) {
			var testLogPath = path.join(os.tmpdir(), 'api-log-error-test.txt'),
				logAPI = new TestAPI({logPath: testLogPath});
			
			logAPI.logError(testMessage).then(function() {
				// Read the log
				return q.nfcall(fs.readFile, testLogPath, {encoding: 'utf8'});
				
			}).then(function(logData) {
				// Verify the error was logged
				assert.equal(logData, 'ERROR ' + testMessage + os.EOL);
				
			}).then(function() {done();}, done).done();
		});
	});
	
	describe('#services', function() {
		var serviceApi;
		
		before('Make a new api instance', function(done) {
			serviceApi = new TestAPI({doSetup: true});
			
			var serviceFolder = path.join(__dirname, 'test-services');
			serviceApi.router.use('/custom-unit-test', serviceApi.services(serviceFolder));
			serviceApi.router.use('*', function(req, res) {
				res.status(200).json({});
			});
			
			serviceApi.listen().then(function() { done(); }, done).done();
		});
		
		it('Adds request context for service handlers', function(done) {
			serviceApi.get('/custom-unit-test/test')
				.then(function() { done(); }, done).done()
		});
		
		it('Returns 404 for non-existent handlers', function(done) {
			serviceApi.get('/custom-unit-test/non-existent-service').then(function() {
				throw new Error('Should not have succeeded');
				
			}, function(err) {
				assert(err.msg.statusCode === 404);
				
			}).then(function() { done(); }, done).done();
		});
	});
});
