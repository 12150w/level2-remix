/*
 * Mailer Tests
 *    Tests the Mailer class
 *
 */
var TestAPI = require('../src/test-api'),
	assert = require('assert');

describe('Mailer', function() {
	var api = new TestAPI({doSetup: true});

	before('Set the Mailer settings', function(done) {
		api.settings.set('email', {
            transport: 'debug',
            options: null
        }).then(function() {done();}, done).done();
	});

	afterEach('Clear any listeners', function() {
		api.mail._eventMap = {};
	});

    describe('send', function() {
		it('Emits the mail event', function(done) {
			this.timeout(250);

			api.mail.on('mail', function(res) {
				try {
					assert.ok(res != null);
				} catch(e) {
					return done(e);
				}

				done();
			});

			api.mail.send({to: 'test@12150w.com'});
		});

		it('Sets the from address', function(done) {
			this.timeout(250);

			api.mail.on('mail', function(res) {
				var body = res.response.toString('utf8');
				try {
					assert.ok(body.indexOf('From: unit-test@12150w.com') > -1, 'From address not set in email');
				} catch(e) {
					return done(e);
				}

				done();
			});

			api.settings.set('email.defaultFrom', 'unit-test@12150w.com').then(function() {
				return api.mail.send({to: 'test@12150w.com'});
			});
		});
    });
});
